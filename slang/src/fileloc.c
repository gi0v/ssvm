#include <stdio.h>
#include <assert.h>
#include <stdarg.h>

#include <fileloc.h>
#include <scompiler.h>

const char *log_lvl_cstr(LogLevel l)
{
    switch (l) {
    case LOG_LVL_ERR: return "error";
    case LOG_LVL_WARN: return "warning";
    case LOG_LVL_NOTE: return "note";
    default: assert(false && "Unreachable.");
    }
}

void log_msg(FileLoc loc, LogLevel lvl, const char *fmt, ...)
{
    if (lvl == LOG_LVL_ERR && !error_hit) error_hit = true;

    fprintf(stderr, FLFmt": %s: ", FLArg(loc), log_lvl_cstr(lvl));

    va_list args;
    va_start(args, fmt);

    vfprintf(stderr, fmt, args);

    va_end(args);

    fprintf(stderr, "\n");

    if (loc.is_macro) {
        loc.is_macro = false;
        {
            log_msg(loc, LOG_LVL_NOTE, "the previous message has happened in the expansion of macro at this location.");
        }
        loc.is_macro = true;
    }
}
