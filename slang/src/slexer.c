#include <ctype.h>
#include <assert.h>

#include <slexer.h>

Lexer *init_lexer(const char *fn)
{
    Lexer *l = calloc(1, sizeof(Lexer));

    if (!str_read_file(STR((char *)fn), &l->src)) {
        free(l);
        return NULL;
    }

    l->glob_loc = (FileLoc) { .x = 1, .y = 1, .fn = fn };

    return l;
}

void free_lexer(Lexer *l)
{
    str_free(l->src);
    free(l);
}

String chop_ln(Lexer *lexer)
{
    String s = str_new();
    char c = str_get_char(lexer->src, 0);

    while (c != '\n') {
        if (c == '\\' && str_get_char(lexer->src, 1) == '\n') {
            lexer->glob_loc.x = 1;
            lexer->glob_loc.y++;
            lexer->src = str_remove_char(lexer->src, 0);
            lexer->src = str_remove_char(lexer->src, 0);
            c = str_get_char(lexer->src, 0);
        }

        s = str_append_char(s, c);

        lexer->src = str_remove_char(lexer->src, 0);
        lexer->glob_loc.x++;

        c = str_get_char(lexer->src, 0);
    }

    return s;
}

void skip_wss(Lexer *lexer)
{
    char c = str_get_char(lexer->src, 0);

    while (c == ' ' || c == '\t' || c == '\n') {
        lexer->src = str_remove_char(lexer->src, 0);
        
        if (c == '\n') {
            lexer->glob_loc.y++;
            lexer->glob_loc.x = 1;
        } else lexer->glob_loc.x++;
        
        c = str_get_char(lexer->src, 0);
    }
}

void skip_comments(Lexer *lexer)
{
    if (str_get_char(lexer->src, 0) == '/' && str_get_char(lexer->src, 1) == '/') {
        while (str_get_char(lexer->src, 0) != '\n') str_remove_char(lexer->src, 0);

        str_remove_char(lexer->src, 0);
        lexer->glob_loc.y++;
        lexer->glob_loc.x = 1;

        skip_comments(lexer);
    }
}

TokenKind id_tok_kind_from_str(String s)
{
    if (STR_CSTR_CMP(s, "func")) return TOK_KIND_FUN;
    else if (STR_CSTR_CMP(s, "if")) return TOK_KIND_IF;
    else if (STR_CSTR_CMP(s, "for")) return TOK_KIND_FOR;
    else if (STR_CSTR_CMP(s, "var")) return TOK_KIND_VAR;
    else if (STR_CSTR_CMP(s, "else")) return TOK_KIND_ELSE;
    else if (STR_CSTR_CMP(s, "while")) return TOK_KIND_WHILE;
    else if (STR_CSTR_CMP(s, "break")) return TOK_KIND_BREAK;
    else if (STR_CSTR_CMP(s, "return")) return TOK_KIND_RET;
    else if (STR_CSTR_CMP(s, "continue")) return TOK_KIND_CONTINUE;
    else return TOK_KIND_ID;
}

Token lex_num(Lexer *lexer)
{
    size_t i = 0;
    String s = str_new();
    char c = str_get_char(lexer->src, i);
    FileLoc loc = lexer->glob_loc;

    while (isdigit(c)) {
        s = str_append_char(s, c);
        c = str_get_char(lexer->src, ++i);
    }

    return (Token) {
        .loc = loc,
        .kind = TOK_KIND_NUM,
        .cost = i,
        .val = s
    };
}

Token lex_id(Lexer *lexer)
{
    size_t i = 0;
    String s = str_new();
    char c = str_get_char(lexer->src, i);
    FileLoc loc = lexer->glob_loc;

    while (isalpha(c) || c == '_' || isdigit(c)) {
        s = str_append_char(s, c);
        c = str_get_char(lexer->src, ++i);
    }

    return (Token) {
        .loc = loc,
        .kind = id_tok_kind_from_str(s),
        .cost = i,
        .val = s
    };
}

char get_eschr(char c)
{
    switch (c) {
    case 'a':  return '\a';
    case 'b':  return '\b';
    case 'n':  return '\n';
    case 'r':  return '\r';
    case 't':  return '\t';
    case 'v':  return '\v';
    case '\\': return '\\';
    case '\'': return '\'';
    case '"':  return '"';
    default:   return c;
    }
}

Token lex_chr(Lexer *lexer)
{
    size_t i = 0;
    String s = str_new();
    FileLoc loc = lexer->glob_loc;

    if (str_get_char(lexer->src, i + 1) == '\\') {
        s = str_append_char(s, get_eschr(str_get_char(lexer->src, i + 2)));
        i += 2;
    } else s = str_append_char(s, str_get_char(lexer->src, ++i));

    i += 2;

    if (str_get_char(lexer->src, i - 1) != '\'') {
        log_msg(loc, LOG_LVL_ERR,
                "expected apostrophe after character literal, but got `%c`.",
                str_get_char(lexer->src, i - 1));
            exit(1);
    }

    return (Token) {
        .loc = loc,
        .kind = TOK_KIND_CHR,
        .cost = i,
        .val = s
    };
}

Token lex_str(Lexer *lexer)
{
    size_t i = 0;
    String s = str_new();
    FileLoc loc = lexer->glob_loc;

    for (i = 1; str_get_char(lexer->src, i) != '"'; ++i) {
        if (str_get_char(lexer->src, i) == '\\') {
            s = str_append_char(s, get_eschr(str_get_char(lexer->src, ++i)));
        } else if (str_get_char(lexer->src, i) == '\n') {
            log_msg(loc, LOG_LVL_WARN, "new line encountered in string, stopping here.");
            break;
        } else s = str_append_char(s, str_get_char(lexer->src, i));
    }

    i++;

    return (Token) {
        .loc = loc,
        .kind = TOK_KIND_STR,
        .cost = i,
        .val = s
    };
}

#define SET_TOK_1(tok, c, k)                              \
    do {                                                  \
        (tok).val = str_append_char((tok).val, (c));      \
        (tok).cost = 1;                                   \
        (tok).kind = (k);                                 \
    } while (false)                                       \

#define SET_TOK_2(tok, c1, c2, k)                          \
    do {                                                   \
        (tok).val = str_append_char((tok).val, (c1));      \
        (tok).val = str_append_char((tok).val, (c2));      \
        (tok).cost = 2;                                    \
        (tok).kind = (k);                                  \
    } while (false)                                        \

#define SET_TOK_3(tok, c1, c2, c3, k)                      \
    do {                                                   \
        (tok).val = str_append_char((tok).val, (c1));      \
        (tok).val = str_append_char((tok).val, (c2));      \
        (tok).val = str_append_char((tok).val, (c3));      \
        (tok).cost = 3;                                    \
        (tok).kind = (k);                                  \
    } while (false)                                        \

static_assert(TOK_COUNT == 54, "Token count changed, "
              "please update the function below.");

Token get_tok(Lexer *lexer)
{
    Token tok = {0};

    skip_wss(lexer);
    skip_comments(lexer);
    skip_wss(lexer);

    char c = str_get_char(lexer->src, 0), c2 = str_get_char(lexer->src, 1), c3 = str_get_char(lexer->src, 2);

    if (isdigit(c)) tok = lex_num(lexer);
    else if (isalpha(c) || c == '_') tok = lex_id(lexer);
    else if (c == '\'') tok = lex_chr(lexer);
    else if (c == '"') tok = lex_str(lexer);
    else {
        tok.val = str_new();
        tok.loc = lexer->glob_loc;

        switch (c) {
        case '+':
            if (c2 == '=') { SET_TOK_2(tok, c, c2, TOK_KIND_ADDSET); }
            else SET_TOK_1(tok, c, TOK_KIND_ADD);
            tok.flags = TOK_FLAG_BIN_OP;
            break;
        case '-':
            if (c2 == '=') { SET_TOK_2(tok, c, c2, TOK_KIND_SUBSET); }
            else SET_TOK_1(tok, c, TOK_KIND_SUB);
            tok.flags = TOK_FLAG_BIN_OP;
            break;
        case '*':
            if (c2 == '=') { SET_TOK_2(tok, c, c2, TOK_KIND_MULSET); }
            else SET_TOK_1(tok, c, TOK_KIND_MUL);
            tok.flags = TOK_FLAG_BIN_OP;
            break;
        case '/':
            if (c2 == '=') { SET_TOK_2(tok, c, c2, TOK_KIND_DIVSET); }
            else SET_TOK_1(tok, c, TOK_KIND_DIV);
            tok.flags = TOK_FLAG_BIN_OP;
            break;
        case '%':
            if (c2 == '=') { SET_TOK_2(tok, c, c2, TOK_KIND_REMSET); }
            else SET_TOK_1(tok, c, TOK_KIND_REM);
            tok.flags = TOK_FLAG_BIN_OP;
            break;
        case '<':
            if (c2 == '<') {
                if (c3 == '=') { SET_TOK_3(tok, c, c2, c3, TOK_KIND_SHLSET); }
                else SET_TOK_2(tok, c, c2, TOK_KIND_SHL);
            }
            else if (c2 == '=') { SET_TOK_2(tok, c, c2, TOK_KIND_LEQ); }
            else { SET_TOK_1(tok, c, TOK_KIND_LES); }
            tok.flags = TOK_FLAG_BIN_OP;
            break;
        case '>':
            if (c2 == '>') {
                if (c3 == '=') { SET_TOK_3(tok, c, c2, c3, TOK_KIND_SHRSET); }
                else SET_TOK_2(tok, c, c2, TOK_KIND_SHR);
            }
            else if (c2 == '=') { SET_TOK_2(tok, c, c2, TOK_KIND_GEQ); }
            else { SET_TOK_1(tok, c, TOK_KIND_GRE); }
            tok.flags = TOK_FLAG_BIN_OP;
            break;
        case '=':
            if (c2 == '=') { SET_TOK_2(tok, c, c2, TOK_KIND_EQU); }
            else { SET_TOK_1(tok, c, TOK_KIND_SET); tok.flags = TOK_FLAG_RL; }
            tok.flags |= TOK_FLAG_BIN_OP;
            break;
        case '!':
            if (c2 == '=') { SET_TOK_2(tok, c, c2, TOK_KIND_NEQ); }
            else { SET_TOK_1(tok, c, TOK_KIND_LNOT); }
            tok.flags = TOK_FLAG_BIN_OP;
            break;
        case '&':
            if (c2 == '&') { SET_TOK_2(tok, c, c2, TOK_KIND_LAND); }
            else if (c2 == '=') { SET_TOK_2(tok, c, c2, TOK_KIND_ANDSET); }
            else { SET_TOK_1(tok, c, TOK_KIND_AMP); }
            tok.flags = TOK_FLAG_BIN_OP;
            break;
        case '^':
            if (c2 == '=') { SET_TOK_2(tok, c, c2, TOK_KIND_XORSET); }
            else SET_TOK_1(tok, c, TOK_KIND_BXOR);
            tok.flags = TOK_FLAG_BIN_OP;
            break;
        case '|':
            if (c2 == '|') { SET_TOK_2(tok, c, c2, TOK_KIND_LOR); }
            else if (c2 == '=') { SET_TOK_2(tok, c, c2, TOK_KIND_ORSET); }
            else { SET_TOK_1(tok, c, TOK_KIND_BOR); }
            tok.flags = TOK_FLAG_BIN_OP;
            break;
        case '(':
            SET_TOK_1(tok, c, TOK_KIND_LPAR);
            break;
        case ')':
            SET_TOK_1(tok, c, TOK_KIND_RPAR);
            break;
        case '[':
            SET_TOK_1(tok, c, TOK_KIND_LBRA);
            break;
        case ']':
            SET_TOK_1(tok, c, TOK_KIND_RBRA);
            break;
        case '{':
            SET_TOK_1(tok, c, TOK_KIND_LCUR);
            break;
        case '}':
            SET_TOK_1(tok, c, TOK_KIND_RCUR);
            break;
        case ':':
            SET_TOK_1(tok, c, TOK_KIND_COL);
            break;
        case ';':
            SET_TOK_1(tok, c, TOK_KIND_SCOL);
            break;
        case ',':
            SET_TOK_1(tok, c, TOK_KIND_COMMA);
            break;
        case '@':
            SET_TOK_1(tok, c, TOK_KIND_AT);
            break;
        case '\0':
            SET_TOK_1(tok, c, TOK_KIND_EOF);
            break;
        default:
            log_msg(lexer->glob_loc, LOG_LVL_ERR, "unknown token `%c`.", c);
            exit(1);
        }
    }

    lexer->last_cost = tok.cost;

    return tok;
}

void next_tok(Lexer *lexer) {
    lexer->glob_loc.x += lexer->last_cost;
    str_free(str_chop_left(&lexer->src, lexer->last_cost));
}

Token expect_tok(Lexer *lexer, TokenKind k)
{
    Token res = get_tok(lexer);

    if (res.kind != k)
        log_msg(res.loc, LOG_LVL_ERR, "was expecting %s, but got %s",
                    tok_kind_to_cstr(k),
                    tok_kind_to_cstr(res.kind));

    next_tok(lexer);

    return res;
}

static_assert(TOK_COUNT == 54, "Token count changed, "
              "please update the function below.");

const char *tok_kind_to_cstr(TokenKind k)
{
    switch (k) {
    case TOK_KIND_ID: return "an identifier";
    case TOK_KIND_IF: return "if";
    case TOK_KIND_AT: return "`@`";
    case TOK_KIND_FOR: return "for";
    case TOK_KIND_BREAK: return "break";
    case TOK_KIND_CONTINUE: return "continue";
    case TOK_KIND_RET: return "return";
    case TOK_KIND_CHR: return "a char";
    case TOK_KIND_STR: return "a string";
    case TOK_KIND_VAR: return "var";
    case TOK_KIND_NUM: return "a number";
    case TOK_KIND_LNOT: return "`!`";
    case TOK_KIND_SET: return "`=`";
    case TOK_KIND_ADDSET: return "`+=`";
    case TOK_KIND_SUBSET: return "`-=`";
    case TOK_KIND_MULSET: return "`*=`";
    case TOK_KIND_DIVSET: return "`/=`";
    case TOK_KIND_REMSET: return "`%=`";
    case TOK_KIND_SHLSET: return "`<<=`";
    case TOK_KIND_SHRSET: return "`>>=`";
    case TOK_KIND_ANDSET: return "`&=`";
    case TOK_KIND_XORSET: return "`^=`";
    case TOK_KIND_ORSET: return "`|=`";
    case TOK_KIND_ADD: return "`+`";
    case TOK_KIND_SUB: return "`-`";
    case TOK_KIND_MUL: return "`*`";
    case TOK_KIND_DIV: return "`/`";
    case TOK_KIND_REM: return "`%`";
    case TOK_KIND_SHL: return "`<<`";
    case TOK_KIND_SHR: return "`>>`";
    case TOK_KIND_GRE: return "`>`";
    case TOK_KIND_GEQ: return "`>=`";
    case TOK_KIND_LES: return "`<`";
    case TOK_KIND_LEQ: return "`<=`";
    case TOK_KIND_EQU: return "`==`";
    case TOK_KIND_NEQ: return "`!=`";
    case TOK_KIND_AMP: return "`&`";
    case TOK_KIND_BXOR: return "`^`";
    case TOK_KIND_BOR: return "`|`";
    case TOK_KIND_LAND: return "`&&`";
    case TOK_KIND_LOR: return "`||`";
    case TOK_KIND_FUN: return "func";
    case TOK_KIND_LPAR: return "`(`";
    case TOK_KIND_RPAR: return "`)`";
    case TOK_KIND_LBRA: return "`[`";
    case TOK_KIND_RBRA: return "`]`";
    case TOK_KIND_LCUR: return "`{`";
    case TOK_KIND_RCUR: return "`}`";
    case TOK_KIND_SCOL: return "`;`";
    case TOK_KIND_ELSE: return "else";
    case TOK_KIND_WHILE: return "while";
    case TOK_KIND_COMMA: return "`,`";
    case TOK_KIND_EOF: return "EOF";
    default: return NULL;
    }
}

void dump_tok(Token tok)
{
    const char t[] = "[%s, \""STRFmt"\", %zu, \""FLNOLFmt"\"]\n";
    printf(t, tok_kind_to_cstr(tok.kind), STRArg(tok.val), tok.cost, FLArg(tok.loc));
}

void free_tok(Token tok)
{
    str_free(tok.val);
}
