#include <sstack.h>

// * Scopes

void push_scope(Compiler *comp, ScopeKind kind)
{
    Scope *scope = calloc(1, sizeof(*scope));
    {
        scope->kind = kind;
        scope->rel_addr = 0;
        scope->parent = comp->scope;
        scope->compiled_loc_vars = init_list(sizeof(CompiledVar *));
    }
    comp->scope = scope;
}

void pop_scope(Compiler *comp)
{
    Scope *tmp = comp->scope->parent;

    free_single_scope(comp->scope);

    comp->scope = tmp;
}

void free_single_scope(Scope *scope)
{
    for (size_t i = 0; i < scope->compiled_loc_vars->items_count; ++i) {
        str_free(((CompiledVar *)scope->compiled_loc_vars->items[i])->name);
        free(scope->compiled_loc_vars->items[i]);
    } free_list(scope->compiled_loc_vars); free(scope);
}

void free_all_scopes(Compiler *comp)
{
    Scope *tmp = comp->scope, *tmp2 = NULL;

    while (tmp != NULL) {
        tmp2 = tmp->parent;

        free_single_scope(tmp);

        tmp = tmp2;
    }
}

Scope *get_proc_scope(Compiler *comp)
{
    Scope *tmp = comp->scope;

    while (tmp != NULL) {
        if (tmp->kind == SCOPE_KIND_PROC)
            return tmp;

        tmp = tmp->parent;
    } assert(false && "Unreachable.");
}

// * Call stack and frames

void setup_call_stack(ssvm_cgen *cgen)
{
    // 1. Set current frame metadata to the global previous frame metadata
    {
        PUSH_INSTR_WARG(cgen, INSTR_PUSH, CURR_FRAME_ADDR);
        PUSH_INSTR_WARG(cgen, INSTR_PUSH, GLOB_PREV_FRAME);
        push_instr(cgen, INSTR_WR64);
    }

    // 2. Set stack size metadata to the value after the global previous frame metadata (PF + 8)
    {
        PUSH_INSTR_WARG(cgen, INSTR_PUSH, STACK_SIZE_ADDR);
        PUSH_INSTR_WARG(cgen, INSTR_PUSH, GLOB_PREV_FRAME + 8);
        push_instr(cgen, INSTR_WR64);
    }

    // 3. Set global previous frame metadata to the address of itself.
    {
        PUSH_INSTR_WARG(cgen, INSTR_PUSH, GLOB_PREV_FRAME);
        PUSH_INSTR_WARG(cgen, INSTR_PUSH, GLOB_PREV_FRAME);
        push_instr(cgen, INSTR_WR64);
    }
}

void push_frame(ssvm_cgen *cgen)
{
    // 1. Write current frame address to stack size's address.
    {
        RD_STACK_SIZE(cgen);
        RD_CURR_FRAME(cgen);
        push_instr(cgen, INSTR_WR64);
    }

    // 2. Write stack size to current frame metadata.
    {
        PUSH_INSTR_WARG(cgen, INSTR_PUSH, CURR_FRAME_ADDR);
        RD_STACK_SIZE(cgen);
        push_instr(cgen, INSTR_WR64);
    }

    // 3. Increment stack size by 8
    INC_STACK_SIZE(cgen, 8);
}

void pop_frame(ssvm_cgen *cgen)
{
    // 1. Write current frame metadata to stack size metadata
    {
        PUSH_INSTR_WARG(cgen, INSTR_PUSH, STACK_SIZE_ADDR);
        RD_CURR_FRAME(cgen);
        push_instr(cgen, INSTR_WR64);
    }

    // 2. Write previous frame address to the current frame metadata
    {
        PUSH_INSTR_WARG(cgen, INSTR_PUSH, CURR_FRAME_ADDR);
        RD_CURR_FRAME(cgen);
        push_instr(cgen, INSTR_RD64);
        push_instr(cgen, INSTR_WR64);
    }
}
