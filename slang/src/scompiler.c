#include <assert.h>
#include <stdbool.h>

#include <sstack.h>
#include <scompiler.h>

bool error_hit = false;

static_assert(BIN_OP_EXPRS_COUNT == 29,
              "Binary operations count changed, "
              "please update the substitution array statement below.");

ssvm_instr_kind bin_op_kind_to_instr[] = {
    [BIN_OP_EXPR_KIND_ADD] = INSTR_ADD,
    [BIN_OP_EXPR_KIND_SUB] = INSTR_SUB,
    [BIN_OP_EXPR_KIND_MUL] = INSTR_MUL,
    [BIN_OP_EXPR_KIND_DIV] = INSTR_DIV,
    [BIN_OP_EXPR_KIND_REM] = INSTR_REM,
    [BIN_OP_EXPR_KIND_SHL] = INSTR_SHL,
    [BIN_OP_EXPR_KIND_SHR] = INSTR_SHR,
    [BIN_OP_EXPR_KIND_GRE] = INSTR_GRE,
    [BIN_OP_EXPR_KIND_GEQ] = INSTR_GEQ,
    [BIN_OP_EXPR_KIND_LES] = INSTR_LES,
    [BIN_OP_EXPR_KIND_LEQ] = INSTR_LEQ,
    [BIN_OP_EXPR_KIND_EQU] = INSTR_EQU,
    [BIN_OP_EXPR_KIND_NEQ] = INSTR_NEQ,
    [BIN_OP_EXPR_KIND_BAND] = INSTR_BAND,
    [BIN_OP_EXPR_KIND_BXOR] = INSTR_BXOR,
    [BIN_OP_EXPR_KIND_BOR] = INSTR_BOR,
    [BIN_OP_EXPR_KIND_LAND] = INSTR_LAND,
    [BIN_OP_EXPR_KIND_LOR] = INSTR_LOR,
    // Assignment expression is handled separately.
};

static ssvm_word main_addr;

Compiler *init_compiler(ssvm_cgen *cgen, ssvm_word mem_cap)
{
    Compiler *comp = calloc(1, sizeof(*comp));
    {
        comp->scope = calloc(1, sizeof(Scope));
        {
            comp->scope->kind = SCOPE_KIND_GENERIC;
            comp->scope->rel_addr = 0;
            comp->scope->parent = NULL;
            comp->scope->compiled_loc_vars = init_list(sizeof(CompiledVar *));
        }

        comp->glob_addr = mem_cap - 1;
        comp->pre_cgen = init_cgen();
        comp->compiled_fundefs = init_list(sizeof(CompiledFunDef *));
        comp->compiled_glob_vars = init_list(sizeof(CompiledVar *));
    }

    CompiledFunDef *dump = push_compiled_fundef(comp, cgen, STR("dump"), 1, type_defs[TYPE_KIND_U0]);
    {
        dump->is_special = true;

        Type *pt = calloc(1, sizeof(*pt));
        {
            *pt = type_defs[TYPE_KIND_U64];
        }
        add_to_list(dump->arg_types, pt);
    }
    CompiledFunDef *write = push_compiled_fundef(comp, cgen, STR("write"), 1, type_defs[TYPE_KIND_U0]);
    {
        write->is_special = true;

        Type *pt = calloc(1, sizeof(*pt));
        {
            *pt = type_defs[TYPE_KIND_U0_PTR];
        }
        add_to_list(write->arg_types, pt);
    }

    return comp;
}

void free_compiler(Compiler *comp)
{
    for (size_t i = 0; i < comp->compiled_fundefs->items_count; ++i) {
        for (size_t j = 0; j < ((CompiledFunDef *)comp->compiled_fundefs->items[i])->arg_types->items_count; ++j) {
            free(((CompiledFunDef *)comp->compiled_fundefs->items[i])->arg_types->items[j]);
        }

        free_list(((CompiledFunDef *)comp->compiled_fundefs->items[i])->arg_types);
        str_free(((CompiledFunDef *)comp->compiled_fundefs->items[i])->name);
        free(comp->compiled_fundefs->items[i]);
    }

    for (size_t i = 0; i < comp->compiled_glob_vars->items_count; ++i) {
        str_free(((CompiledVar *)comp->compiled_glob_vars->items[i])->name);
        free(comp->compiled_glob_vars->items[i]);
    }

    free_all_scopes(comp);
    free_cgen(comp->pre_cgen);
    free_list(comp->compiled_fundefs);
    free_list(comp->compiled_glob_vars);
    free(comp);
}

void push_compile_time_str_lit(Compiler *comp, ssvm_cgen *cgen, String s)
{
    comp->glob_addr -= ((s.length + 1) * sizeof(char)); // Full size (length + null character) * char_size

    PUSH_INSTR_WARG(cgen, INSTR_PUSH, comp->glob_addr);

    for (size_t i = 0; i <= s.length; ++i) {
        PUSH_INSTR_WARG(cgen, INSTR_PUSH, comp->glob_addr + i);
        PUSH_INSTR_WARG(cgen, INSTR_PUSH, str_get_char(s, i));
        push_instr(cgen, INSTR_WR8);
    }
}

CompiledFunDef *push_compiled_fundef(Compiler *comp, ssvm_cgen *cgen, String name, size_t arity, Type ret_type)
{
    CompiledFunDef *cfundef = calloc(1, sizeof(*cfundef));
    {
        cfundef->name = str_from_static_cstr(STR_CSTR(name));
        cfundef->addr = cgen->instr_addr;
        cfundef->is_special = false;
        cfundef->arg_types = init_list(sizeof(Type *));
        cfundef->arity = arity;
        cfundef->ret_type = ret_type;
    }
    add_to_list(comp->compiled_fundefs, cfundef);

    return cfundef;
}

// Expects only number literals and binary operations
uint64_t eval_expr(FileLoc l, Expr expr)
{
    if (expr.kind == EXPR_KIND_NUM) {
        return expr.as.num;
    }

    uint64_t a = eval_expr(l, expr.as.bin_op->lhs);
    uint64_t b = eval_expr(l, expr.as.bin_op->rhs);

    switch (expr.as.bin_op->kind) {
    case BIN_OP_EXPR_KIND_ADD:  return a +  b;
    case BIN_OP_EXPR_KIND_SUB:  return a -  b;
    case BIN_OP_EXPR_KIND_MUL:  return a *  b;
    case BIN_OP_EXPR_KIND_DIV:  return a /  b;
    case BIN_OP_EXPR_KIND_REM:  return a %  b;
    case BIN_OP_EXPR_KIND_SHL:  return a << b;
    case BIN_OP_EXPR_KIND_SHR:  return a >> b;
    case BIN_OP_EXPR_KIND_GRE:  return a >  b;
    case BIN_OP_EXPR_KIND_GEQ:  return a >= b;
    case BIN_OP_EXPR_KIND_LES:  return a <  b;
    case BIN_OP_EXPR_KIND_LEQ:  return a <= b;
    case BIN_OP_EXPR_KIND_EQU:  return a == b;
    case BIN_OP_EXPR_KIND_NEQ:  return a != b;
    case BIN_OP_EXPR_KIND_BAND: return a &  b;
    case BIN_OP_EXPR_KIND_BXOR: return a ^  b;
    case BIN_OP_EXPR_KIND_BOR:  return a |  b;
    case BIN_OP_EXPR_KIND_LAND: return a && b;
    case BIN_OP_EXPR_KIND_LOR:  return a || b;
    default:
        log_msg(l, LOG_LVL_ERR, "invalid expression for array declaration.");
        exit(1);
    }
}

CompiledVar push_compile_time_var(Compiler *comp, ssvm_cgen *cgen, VarDef var_def, bool is_glob)
{
    CompiledVar *var = NULL;

    if (is_glob) {
        if (var_def.var_expr.kind == EXPR_KIND_ID) {
            if (search_local_var(comp, var_def.var_expr.as.id) != NULL)
                log_msg(var_def.loc, LOG_LVL_WARN, "redeclaration of local variable as global variable.");
            else if (search_global_var(comp, var_def.var_expr.as.id) != NULL)
                log_msg(var_def.loc, LOG_LVL_WARN, "redeclaration of global variable.");

            comp->glob_addr -= var_def.type.sz;

            var = calloc(1, sizeof(*var));
            {
                var->as.glob_var = comp->glob_addr;
                var->name = str_from_static_cstr(STR_CSTR(var_def.var_expr.as.id));
                var->type = var_def.type;
            }
            add_to_list(comp->compiled_glob_vars, var);

            if (var_def.has_init) {
                PUSH_INSTR_WARG(comp->pre_cgen, INSTR_PUSH, var->as.glob_var);
                compile_expr(comp, comp->pre_cgen, var_def.init, REQ_NONE, 1);
                push_instr(comp->pre_cgen, var->type.as.instr.wr);
            }
        } else if (var_def.var_expr.kind == EXPR_KIND_INDEX) {
            if (search_local_var(comp, var_def.var_expr.as.index->id) != NULL)
                log_msg(var_def.loc, LOG_LVL_WARN, "redeclaration of local variable as global variable.");
            else if (search_global_var(comp, var_def.var_expr.as.index->id) != NULL)
                log_msg(var_def.loc, LOG_LVL_WARN, "redeclaration of global variable.");

            size_t count = eval_expr(var_def.var_expr.loc, var_def.var_expr.as.index->index);

            if (count <= 1)
                log_msg(var_def.var_expr.loc, LOG_LVL_ERR, "array elements count has to be greater than 1.");

            comp->glob_addr -= 8;
            var = calloc(1, sizeof(*var));
            {
                var->as.glob_var = comp->glob_addr;
                var->name = str_from_static_cstr(STR_CSTR(var_def.var_expr.as.index->id));
                var->type = type_defs[var_def.type.as.ptr];
            }
            add_to_list(comp->compiled_glob_vars, var);
            comp->glob_addr -= count * var_def.type.sz;

            // Set pointer to beginning of array.
            {
                PUSH_INSTR_WARG(comp->pre_cgen, INSTR_PUSH, var->as.glob_var);
                PUSH_INSTR_WARG(comp->pre_cgen, INSTR_PUSH, comp->glob_addr);
                push_instr(comp->pre_cgen, var->type.as.instr.wr);
            }

            if (var_def.has_init)
                log_msg(var_def.init.loc, LOG_LVL_WARN, "cannot initialize array, ignoring expression.");
        } else { log_msg(var_def.var_expr.loc, LOG_LVL_ERR, "invalid expression for variable declaration."); exit(1); }
    } else {
        Scope *pscope = get_proc_scope(comp);

        if (var_def.var_expr.kind == EXPR_KIND_ID) {
            if (search_local_var(comp, var_def.var_expr.as.id) != NULL)
                log_msg(var_def.loc, LOG_LVL_WARN, "redeclaration of local variable.");
            else if (search_global_var(comp, var_def.var_expr.as.id) != NULL)
                log_msg(var_def.loc, LOG_LVL_WARN, "redeclaration of global variable as local variable.");

            var = calloc(1, sizeof(*var));
            {
                var->as.loc_var = pscope->rel_addr;
                var->name = str_from_static_cstr(STR_CSTR(var_def.var_expr.as.id));
                var->type = var_def.type;
            }
            add_to_list(pscope->compiled_loc_vars, var);
            
            pscope->rel_addr += var_def.type.sz;
            INC_STACK_SIZE(cgen, var_def.type.sz);

            if (var_def.has_init) {
                push_local_var_addr(cgen, *var);
                compile_expr(comp, cgen, var_def.init, REQ_NONE, 1);
                push_instr(cgen, var->type.as.instr.wr);
            }
        } else if (var_def.var_expr.kind == EXPR_KIND_INDEX) {
            if (search_local_var(comp, var_def.var_expr.as.index->id) != NULL)
                log_msg(var_def.loc, LOG_LVL_WARN, "redeclaration of local variable.");
            else if (search_global_var(comp, var_def.var_expr.as.index->id) != NULL)
                log_msg(var_def.loc, LOG_LVL_WARN, "redeclaration of global variable as local variable.");

            size_t count = eval_expr(var_def.var_expr.loc, var_def.var_expr.as.index->index);

            if (count <= 1)
                log_msg(var_def.var_expr.loc, LOG_LVL_ERR, "array elements count has to be greater than 1.");

            pscope->rel_addr += count * var_def.type.sz;
            var = calloc(1, sizeof(*var));
            {
                var->as.loc_var = pscope->rel_addr;
                var->name = str_from_static_cstr(STR_CSTR(var_def.var_expr.as.index->id));
                var->type = type_defs[var_def.type.as.ptr];
            }
            add_to_list(pscope->compiled_loc_vars, var);
            pscope->rel_addr += 8;

            // 1. Set pointer to beginning of array.
            {
                push_local_var_addr(cgen, *var);
                RD_STACK_SIZE(cgen);
                push_instr(cgen, var->type.as.instr.wr);
            }

            // 2. Increment stack size.
            INC_STACK_SIZE(cgen, count * var_def.type.sz + 8);

            if (var_def.has_init)
                log_msg(var_def.init.loc, LOG_LVL_WARN, "cannot initialize array, ignoring expression.");
        } else { log_msg(var_def.var_expr.loc, LOG_LVL_ERR, "invalid expression for variable declaration."); exit(1); }
    }

    return *var;
}

void push_local_var_addr(ssvm_cgen *cgen, CompiledVar var)
{
    RD_CURR_FRAME(cgen);
    PUSH_INSTR_WARG(cgen, INSTR_PUSH, var.as.loc_var + 8); // + 8 to account for the frame metadata
    push_instr(cgen, INSTR_ADD);
}

CompiledVar *search_local_var(Compiler *comp, String name)
{
    Scope *tmp = comp->scope;

    while (tmp != NULL) {
        for (size_t i = 0; i < tmp->compiled_loc_vars->items_count; ++i)
            if (STR_CMP(((CompiledVar *)tmp->compiled_loc_vars->items[i])->name, name))
                return tmp->compiled_loc_vars->items[i];

        tmp = tmp->parent;
    }

    return NULL;
}

CompiledVar *search_global_var(Compiler *comp, String name)
{
    for (size_t i = 0; i < comp->compiled_glob_vars->items_count; ++i)
        if (STR_CMP(((CompiledVar *)comp->compiled_glob_vars->items[i])->name, name))
            return comp->compiled_glob_vars->items[i];

    return NULL;
}

void compile_program_to_file(Lexer *lexer, const char *fn, size_t mem_sz, bool dump_ast, bool run_prog)
{
    ssvm_word pre_addr = 0;
    ssvm_cgen *cgen = init_cgen();
    Compiler *comp = init_compiler(cgen, mem_sz);

    // Pre-execution code (only global variable arrays for now)
    {
        push_instr(cgen, INSTR_JSR);
        pre_addr = cgen->curr_addr; // To be set later when main() is defined.
        push_instr_arg(cgen, 0);
    }

    setup_call_stack(cgen);

    push_frame(cgen);
    {
        push_instr(cgen, INSTR_JSR);
        main_addr = cgen->curr_addr; // To be set later when main() is defined.
        push_instr_arg(cgen, 0);
    }
    pop_frame(cgen);

    push_instr(cgen, INSTR_HLT);

    // Push prepended code
    ssvm_word addr = cgen->instr_addr;
    SET_ARRAY64_ENDIANNESS(cgen->prog, pre_addr, addr);
    {
        for (size_t i = 0; i < comp->pre_cgen->prog_sz; ++i)
            push_instr(cgen, comp->pre_cgen->prog[i]);

        push_instr(cgen, INSTR_RET);
    }

    compile_tops_until_eof(comp, cgen, lexer, dump_ast);

    if (error_hit) goto end;

    if (run_prog) {
        ssvm *ssvm = init_ssvm(mem_sz);

        ld_ssvm_program(ssvm, cgen->prog, cgen->prog_sz);
        exec_until_hlt(ssvm);

        free_ssvm(ssvm);
    } else wr_cgen_to_file(cgen, fn, mem_sz);

 end:
    free_cgen(cgen);
    free_compiler(comp);
}

void compile_tops_until_eof(Compiler *comp, ssvm_cgen *cgen, Lexer *lexer, bool dump_ast)
{
    Top top = {0};
    Token peek = {0};

    while (peek.kind != TOK_KIND_EOF) {
        top = prs_top(lexer);

        compile_top(comp, cgen, top, dump_ast);

        free_top(top);
        free_tok(peek);

        peek = get_tok(lexer);
    } free_tok(peek);
}

static_assert(TOPS_COUNT == 4,
              "Top statements count has changed, "
              "please update the function below.");

void compile_top(Compiler *comp, ssvm_cgen *cgen, Top top, bool dump_ast)
{
    switch (top.kind) {

    case TOP_KIND_FUN_DEF: {
        if (dump_ast) dump_top(top);

        VarDef **args = (VarDef **)(top.as.fundef.args->items);
        size_t args_count = top.as.fundef.args->items_count;

        for (size_t i = 0; i < comp->compiled_fundefs->items_count; ++i)
            if (STR_CMP(((CompiledFunDef *)comp->compiled_fundefs->items[i])->name,
                        top.as.fundef.name))
                log_msg(top.loc, LOG_LVL_WARN, "redeclaration of function `"STRFmt"`.",
                        STRArg(top.as.fundef.name));

        CompiledFunDef *cfundef = push_compiled_fundef(comp, cgen,
                                                       top.as.fundef.name, args_count,
                                                       top.as.fundef.ret_type);

        if (STR_CSTR_CMP(cfundef->name, "main")) {
            ssvm_word addr = cfundef->addr;

            SET_ARRAY64_ENDIANNESS(cgen->prog, main_addr, addr);
        }

        push_scope(comp, SCOPE_KIND_PROC);
        {
            for (size_t i = args_count - 1; i + 1 > 0; --i) {
                CompiledVar var = push_compile_time_var(comp, cgen, *args[i], false);

                Type *pt = calloc(1, sizeof(*pt));
                {
                    *pt = args[(args_count - 1) - i]->type;
                }
                add_to_list(cfundef->arg_types, pt);
                
                push_instr(cgen, INSTR_SWAP);
                push_local_var_addr(cgen, var);
                push_instr(cgen, INSTR_SWAP);
                push_instr(cgen, var.type.as.instr.wr);
            }

            compile_stmt(comp, cgen, top.as.fundef.body);
        }
        pop_scope(comp);
        
        push_instr(cgen, INSTR_RET);
    } break;

    case TOP_KIND_VAR_DEF: {
        // Global variable

        if (dump_ast) dump_top(top);

        push_compile_time_var(comp, cgen, top.as.var_def, true);
    } break;

    case TOP_KIND_INCLUDE: {
        Lexer *l2 = init_lexer(STR_CSTR(top.as.include));

        if (l2 == NULL)
            log_msg(top.loc, LOG_LVL_ERR, "failed to open file `"STRFmt"`.", STRArg(top.as.include));
        else {
            compile_tops_until_eof(comp, cgen, l2, dump_ast);
            free_lexer(l2);
        }
    } break;

    case TOP_KIND_MACRO: return;

    default:
        assert(false && "Unreachable.");
    }
}

bool is_inside_kind_scope(Compiler *comp, ScopeKind k)
{
    Scope *s = comp->scope;

    while (s != NULL) {
        if (s->kind == k) return true;

        s = s->parent;
    } return false;
}

static_assert(STMTS_COUNT == 9,
              "Statements count changed, "
              "please update the function below.");

void compile_stmt(Compiler *comp, ssvm_cgen *cgen, Stmt stmt)
{
    switch (stmt.kind) {

    case STMT_KIND_COMPOUND: {
        push_scope(comp, SCOPE_KIND_GENERIC);
        {
            for (size_t i = 0; i < stmt.as.compound->items_count; ++i)
                compile_stmt(comp, cgen, *((Stmt *) stmt.as.compound->items[i]));
        }
        pop_scope(comp);
    } break;

    case STMT_KIND_EXPR: {
        compile_expr(comp, cgen, stmt.as.expr, REQ_NONE, 0);
    } break;

    case STMT_KIND_IF: {
        ssvm_word else_addr = 0, end_addr = 0;

        // 1. Negate condition and prepend conditional jump to end of statement
        {
            compile_expr(comp, cgen, stmt.as.if_stmt.cond, REQ_NONE, 1);
            push_instr(cgen, INSTR_LNOT);

            push_instr(cgen, INSTR_JIF);

            else_addr = cgen->curr_addr;
            push_instr_arg(cgen, 0);
        }

        // 2. Compile body
        {
            compile_stmt(comp, cgen, *stmt.as.if_stmt.body);
            push_instr(cgen, INSTR_JMP);

            end_addr = cgen->curr_addr;
            push_instr_arg(cgen, 0);
        }

        // 3. Compile else
        {
            ssvm_word a = cgen->instr_addr;
            SET_ARRAY64_ENDIANNESS(cgen->prog, else_addr, a);

            if (stmt.as.if_stmt.else_s != NULL)
                compile_stmt(comp, cgen, *stmt.as.if_stmt.else_s);
        }

        // 4. Set statement end address.
        {
            ssvm_word a = cgen->instr_addr;
            SET_ARRAY64_ENDIANNESS(cgen->prog, end_addr, a);
        }
    } break;

    case STMT_KIND_FOR: {
        push_scope(comp, SCOPE_KIND_LOOP);
        {
            compile_stmt(comp, cgen, *stmt.as.for_l.sstmt);
            
            ssvm_word cond_addr, end_addr, ls_addr, le_addr;
            
            // 1. Negate condition and prepend conditional jump to end of statement
            {
                cond_addr = cgen->instr_addr;
                
                compile_expr(comp, cgen, stmt.as.for_l.cond, REQ_NONE, 1);
                push_instr(cgen, INSTR_LNOT);
                
                push_instr(cgen, INSTR_JIF);
                
                end_addr = cgen->curr_addr;
                push_instr_arg(cgen, 0);
            }

            // 2. Push loop metadata
            {
                push_instr(cgen, INSTR_PUSH);
                ls_addr = cgen->curr_addr;
                push_instr_arg(cgen, 0);

                push_instr(cgen, INSTR_PUSH);
                le_addr = cgen->curr_addr;
                push_instr_arg(cgen, 0);
            }
            
            // 3. Compile body along with finishing statement
            {
                compile_stmt(comp, cgen, *stmt.as.for_l.body);

                // NOTE: COST_CALC(0) is the cost of the RET instruction,
                // it is substracted from the current address because fwd_instr()
                // increments the program counter after executing the RET.
                ssvm_word a = cgen->curr_addr - COST_CALC(0); 
                SET_ARRAY64_ENDIANNESS(cgen->prog, ls_addr, a);
                compile_stmt(comp, cgen, *stmt.as.for_l.fstmt);

                PUSH_INSTR_WARG(cgen, INSTR_DROP, 1);
                PUSH_INSTR_WARG(cgen, INSTR_JMP, cond_addr);
            }
            
            // 4. Set jump address
            {
                ssvm_word a = cgen->instr_addr;
                SET_ARRAY64_ENDIANNESS(cgen->prog, end_addr, a);
                a = cgen->curr_addr - COST_CALC(0);
                SET_ARRAY64_ENDIANNESS(cgen->prog, le_addr, a);
            }
        }
        pop_scope(comp);
    } break;

    case STMT_KIND_WHILE: {
        push_scope(comp, SCOPE_KIND_LOOP);
        {
            ssvm_word cond_addr, end_addr, ls_addr, le_addr;

            // 1. Negate condition and prepend conditional jump to end of statement
            {
                ls_addr = cgen->curr_addr;
                cond_addr = cgen->instr_addr;
                
                compile_expr(comp, cgen, stmt.as.while_l.cond, REQ_NONE, 1);
                push_instr(cgen, INSTR_LNOT);
                
                push_instr(cgen, INSTR_JIF);
                
                end_addr = cgen->curr_addr;
                push_instr_arg(cgen, 0);
            }

            // 2. Push loop metadata
            {
                push_instr(cgen, INSTR_PUSH);
                ls_addr = cgen->curr_addr;
                push_instr_arg(cgen, 0);

                push_instr(cgen, INSTR_PUSH);
                le_addr = cgen->curr_addr;
                push_instr_arg(cgen, 0);
            }
            
            // 3. Compile body
            {
                compile_stmt(comp, cgen, *stmt.as.while_l.body);

                ssvm_word a = cgen->curr_addr - COST_CALC(0);
                SET_ARRAY64_ENDIANNESS(cgen->prog, ls_addr, a);

                PUSH_INSTR_WARG(cgen, INSTR_DROP, 1);
                PUSH_INSTR_WARG(cgen, INSTR_JMP, cond_addr);
            }
            
            // 4. Set jump addresses
            {
                ssvm_word a = cgen->instr_addr;
                SET_ARRAY64_ENDIANNESS(cgen->prog, end_addr, a);
                a = cgen->curr_addr - COST_CALC(0);
                SET_ARRAY64_ENDIANNESS(cgen->prog, le_addr, a);
            }
        }
        pop_scope(comp);
    } break;

    case STMT_KIND_RETURN: {
        if (is_inside_kind_scope(comp, SCOPE_KIND_LOOP))
            PUSH_INSTR_WARG(cgen, INSTR_DROP, 1);

        Type t = compile_expr(comp, cgen, stmt.as.ret, REQ_NONE, 1);

        type_chk_expr(stmt.as.ret.loc,
                      ((CompiledFunDef *)
                       comp->compiled_fundefs->items[comp->compiled_fundefs->items_count - 1])->ret_type, t);

        if (t.sz > 0)
            push_instr(cgen, INSTR_SWAP);

        push_instr(cgen, INSTR_RET);
    } break;

    case STMT_KIND_BREAK: {
        if (!is_inside_kind_scope(comp, SCOPE_KIND_LOOP))
            log_msg(stmt.loc, LOG_LVL_ERR, "break outside of loop.");
        
        push_instr(cgen, INSTR_SWAP);
        PUSH_INSTR_WARG(cgen, INSTR_DROP, 0);
        push_instr(cgen, INSTR_RET);
    } break;

    case STMT_KIND_CONTINUE: {
        if (!is_inside_kind_scope(comp, SCOPE_KIND_LOOP))
            log_msg(stmt.loc, LOG_LVL_ERR, "continue outside of loop.");
        
        push_instr(cgen, INSTR_SWAP);
        PUSH_INSTR_WARG(cgen, INSTR_DUP, 0);
        push_instr(cgen, INSTR_RET);
    } break;

    case STMT_KIND_VAR_DEF: {
        // Local variable

        push_compile_time_var(comp, cgen, stmt.as.var_def, false);
    } break;

    default:
        assert(false && "Unreachable.");
    }
}

Type num_to_type(uint64_t n)
{
    if (n > UINT8_MAX)
        if (n > UINT16_MAX)
            if (n > UINT32_MAX)
                return type_defs[TYPE_KIND_U64];
            else return type_defs[TYPE_KIND_U32];
        else return type_defs[TYPE_KIND_U16];
    else return type_defs[TYPE_KIND_U8];
}

#define IS_PTR(t) ((t).kind == (t).as.ptr)
#define IS_NOT_PTR(t) ((t).kind == (t).as.non_ptr)

bool type_chk(Type t1, Type t2)
{
    // NOTE: t1 is considered the *main* type, so it's really
    // t2 that has to be compatible with t1 and NOT vice versa.

    if (t1.kind == t2.kind) return true;

    if (t1.sz == 0 || t2.sz == 0) return false;

    if ((IS_PTR(t1) && IS_PTR(t2))
        && ((t1.kind == TYPE_KIND_U0_PTR) || (t2.kind == TYPE_KIND_U0_PTR))) return true;

    if ((IS_NOT_PTR(t1) && IS_NOT_PTR(t2)) && (t1.sz > t2.sz)) return true;

    return false;
}

Type type_chk_expr(FileLoc loc, Type t1, Type t2)
{
    if (!type_chk(t1, t2))
        log_msg(loc, LOG_LVL_ERR, "incompatible types between %s and %s.", t1.name, t2.name);

    return t1;
}

#define BIN_OP_EXPR_SET_BY_OP(comp, cgen, expr, depth, op)              \
    do {                                                                \
        Type t1 = compile_expr(comp, cgen, expr.as.bin_op->lhs,         \
                               REQ_PUSH_VAR_ADDR, depth + 1);           \
        PUSH_INSTR_WARG(cgen, INSTR_DUP, 0);                            \
        push_instr(cgen, t1.as.instr.rd);                               \
        Type t2 = compile_expr(comp, cgen, expr.as.bin_op->rhs,         \
                               REQ_NONE, depth + 1);                    \
        push_instr(cgen, op);                                           \
                                                                        \
        type_chk_expr(expr.as.bin_op->loc, t1, t2);                     \
                                                                        \
        if (depth > 0) PUSH_INSTR_WARG(cgen, INSTR_DUP, 0);             \
                                                                        \
        push_instr(cgen, t1.as.instr.wr);                               \
                                                                        \
        if (depth > 0) {                                                \
            push_instr(cgen, INSTR_ROT);                                \
            push_instr(cgen, INSTR_SWAP);                               \
        }                                                               \
                                                                        \
        return t1;                                                      \
    } while (false)                                                     \

static_assert(EXPRS_COUNT == 8, "Expression count changed, "
              "please update the function below.");

Type compile_expr(Compiler *comp, ssvm_cgen *cgen, Expr expr, CompilerReq req, size_t depth)
{
    switch (expr.kind) {
    case EXPR_KIND_NUM:
        if (depth == 0) return type_defs[TYPE_KIND_U0];

        PUSH_INSTR_WARG(cgen, INSTR_PUSH, expr.as.num);

        return num_to_type(expr.as.num);
    case EXPR_KIND_STR:
        if (depth == 0) return type_defs[TYPE_KIND_U0];

        push_compile_time_str_lit(comp, cgen, expr.as.str);

        return type_defs[TYPE_KIND_U8_PTR];

    case EXPR_KIND_INDEX: {
        if (depth == 0) return type_defs[TYPE_KIND_U0];

        Expr e = (Expr) {
            .loc = expr.loc,
            .kind = EXPR_KIND_ID,
            .as.id = expr.as.index->id
        };

        Type t = type_defs[compile_expr(comp, cgen, e, REQ_NONE, depth).as.non_ptr];

        compile_expr(comp, cgen, expr.as.index->index, REQ_NONE, depth);
        PUSH_INSTR_WARG(cgen, INSTR_PUSH, t.sz);
        push_instr(cgen, INSTR_MUL);
        push_instr(cgen, INSTR_ADD);

        if (!(req & REQ_PUSH_VAR_ADDR)) {
            ASSERT(t.sz != 0, expr.loc,
                   "cannot perform a read operation on type %s.", type_defs[t.as.non_ptr].name);
            push_instr(cgen, t.as.instr.rd);
        }

        return t;
    }

    case EXPR_KIND_ID: {
        if (depth == 0) return type_defs[TYPE_KIND_U0];

        CompiledVar var, *tmp;

        if ((tmp = search_local_var(comp, expr.as.id)) != NULL) {
            // Local variable

            var = *tmp;
            push_local_var_addr(cgen, var);
        } else if ((tmp = search_global_var(comp, expr.as.id)) != NULL) {
            // Global variable

            var = *tmp;
            PUSH_INSTR_WARG(cgen, INSTR_PUSH, var.as.glob_var);
        } else {
            log_msg(expr.loc, LOG_LVL_ERR,
                    "variable `"STRFmt"` not found in the current scope.", STRArg(expr.as.id));
        } 
        
        if (!(req & REQ_PUSH_VAR_ADDR)) {
            ASSERT(var.type.sz != 0, expr.loc,
                   "cannot perform a read operation on %s type.", var.type.name);
            push_instr(cgen, var.type.as.instr.rd);
        }

        return var.type;
    }

    case EXPR_KIND_FUN_CALL:
        if (STR_CSTR_CMP(expr.as.funcall->name, "cast")) {
            if (expr.as.funcall->args->items_count != 2)
                log_msg(expr.loc, LOG_LVL_ERR, "cast() expects an expression and a type.");
            else {
                compile_expr(comp, cgen, *(Expr *)expr.as.funcall->args->items[0], req, depth);
                
                if (((Expr *)expr.as.funcall->args->items[1])->kind == EXPR_KIND_ID)
                    return str_to_type(((Expr *)expr.as.funcall->args->items[1])->loc,
                                       ((Expr *)expr.as.funcall->args->items[1])->as.id);
                else log_msg(expr.loc, LOG_LVL_ERR, "cast() expects an expression and a type.");
            }

            return type_defs[TYPE_KIND_U0];
        }

        for (size_t i = 0; i < comp->compiled_fundefs->items_count; ++i) {
            CompiledFunDef fundef = *((CompiledFunDef *)comp->compiled_fundefs->items[i]);
            
            if (STR_CMP(expr.as.funcall->name, fundef.name)) {
                Type t = {0};

                if (fundef.arity != expr.as.funcall->args->items_count) {
                    log_msg(expr.loc, LOG_LVL_ERR,
                            "function arity is %zu when expected one is %zu.",
                            expr.as.funcall->args->items_count, fundef.arity);

                    goto ret;
                }
                

                for (size_t i = 0; i < expr.as.funcall->args->items_count; ++i) {
                    t = compile_expr(comp, cgen, *((Expr *) expr.as.funcall->args->items[i]), REQ_NONE, depth + 1);
                    t = type_chk_expr(((Expr *) expr.as.funcall->args->items[i])->loc,
                                      *((Type *)fundef.arg_types->items[i]), t);
                }
                
                if (fundef.is_special) {
                    if (STR_CSTR_CMP(fundef.name, "dump")) PUSH_INSTR_WARG(cgen, INSTR_NAT, NAT_DUMP);
                    else if (STR_CSTR_CMP(fundef.name, "write")) PUSH_INSTR_WARG(cgen, INSTR_NAT, NAT_WRITE);
                } else {
                    push_frame(cgen);
                    {
                        PUSH_INSTR_WARG(cgen, INSTR_JSR, fundef.addr);
                    }
                    pop_frame(cgen);
                }

                if (depth == 0 && fundef.ret_type.kind != TYPE_KIND_U0)
                    PUSH_INSTR_WARG(cgen, INSTR_DROP, 0);

                return fundef.ret_type;
            }
        }
        
        log_msg(expr.loc, LOG_LVL_ERR, "function `"STRFmt"` not found.", STRArg(expr.as.funcall->name));

    ret:

        return type_defs[TYPE_KIND_U0];
    case EXPR_KIND_UNARY_OP:
        if (depth == 0) return type_defs[TYPE_KIND_U0];

        static_assert(UNARY_OP_EXPRS_COUNT == 2, "Unary operations count changed, "
                      "please update the switch statement below.");

        switch (expr.as.unary_op->kind) {
        case UNARY_OP_EXPR_KIND_ADDR_OF:
            return type_defs[compile_expr(comp, cgen, expr.as.unary_op->rhs,
                                          REQ_PUSH_VAR_ADDR, depth).as.ptr];
        case UNARY_OP_EXPR_KIND_DEREF: {
            Type t = type_defs[compile_expr(comp, cgen, expr.as.unary_op->rhs, REQ_NONE, depth).as.non_ptr];

            ASSERT(t.sz != 0, expr.loc, "dereferencing void pointer.");

            if (!(req & REQ_PUSH_VAR_ADDR)) push_instr(cgen, t.as.instr.rd);

            return t;
        }
        default:
            assert(false && "Unreachable.");
        }
    case EXPR_KIND_BIN_OP:
        switch (expr.as.bin_op->kind) {

        case BIN_OP_EXPR_KIND_SET: {
            Type t1 = compile_expr(comp, cgen, expr.as.bin_op->lhs, REQ_PUSH_VAR_ADDR, depth + 1);
            Type t2 = compile_expr(comp, cgen, expr.as.bin_op->rhs, REQ_NONE, depth + 1);

            type_chk_expr(expr.as.bin_op->loc, t1, t2);

            if (depth > 0) PUSH_INSTR_WARG(cgen, INSTR_DUP, 0);

            push_instr(cgen, t1.as.instr.wr);

            if (depth > 0) { push_instr(cgen, INSTR_ROT); push_instr(cgen, INSTR_SWAP);}

            return t1;
        }

        case BIN_OP_EXPR_KIND_ADDSET:
            { BIN_OP_EXPR_SET_BY_OP(comp, cgen, expr, depth, INSTR_ADD); }
        case BIN_OP_EXPR_KIND_SUBSET:
            { BIN_OP_EXPR_SET_BY_OP(comp, cgen, expr, depth, INSTR_SUB); }
        case BIN_OP_EXPR_KIND_MULSET:
            { BIN_OP_EXPR_SET_BY_OP(comp, cgen, expr, depth, INSTR_MUL); }
        case BIN_OP_EXPR_KIND_DIVSET:
            { BIN_OP_EXPR_SET_BY_OP(comp, cgen, expr, depth, INSTR_DIV); }
        case BIN_OP_EXPR_KIND_REMSET:
            { BIN_OP_EXPR_SET_BY_OP(comp, cgen, expr, depth, INSTR_REM); }
        case BIN_OP_EXPR_KIND_SHLSET:
            { BIN_OP_EXPR_SET_BY_OP(comp, cgen, expr, depth, INSTR_SHL); }
        case BIN_OP_EXPR_KIND_SHRSET:
            { BIN_OP_EXPR_SET_BY_OP(comp, cgen, expr, depth, INSTR_SHR); }
        case BIN_OP_EXPR_KIND_ANDSET:
            { BIN_OP_EXPR_SET_BY_OP(comp, cgen, expr, depth, INSTR_BAND); }
        case BIN_OP_EXPR_KIND_XORSET:
            { BIN_OP_EXPR_SET_BY_OP(comp, cgen, expr, depth, INSTR_BXOR); }
        case BIN_OP_EXPR_KIND_ORSET:
            { BIN_OP_EXPR_SET_BY_OP(comp, cgen, expr, depth, INSTR_BOR); }

        default: {
            if (depth == 0) return type_defs[TYPE_KIND_U0];

            Type t1 = compile_expr(comp, cgen, expr.as.bin_op->lhs, REQ_NONE, depth + 1);
            Type t2 = compile_expr(comp, cgen, expr.as.bin_op->rhs, REQ_NONE, depth + 1);
            
            push_instr(cgen, bin_op_kind_to_instr[expr.as.bin_op->kind]);

            return (t1.sz > t2.sz ? t1 : t2);
        }

        }

    case EXPR_KIND_NOP:
        push_instr(cgen, INSTR_NOP);
        return type_defs[TYPE_KIND_U0];

    default:
        assert(false && "Unreachable.");
    }
}
