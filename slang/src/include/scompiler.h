#ifndef _SCOMPILER_H
#define _SCOMPILER_H

#include <ssvm.h>
#include <sprs.h>

typedef struct Scope Scope;

typedef struct {
    ssvm_word glob_addr;
    ssvm_cgen *pre_cgen;
    List *compiled_fundefs;   // List : CompiledFunDef
    List *compiled_glob_vars; // List : CompiledVar
    Scope *scope;
} Compiler;

typedef enum {
    SCOPE_KIND_GENERIC = 0,
    SCOPE_KIND_PROC,
    SCOPE_KIND_LOOP,
} ScopeKind;

struct Scope {
    ScopeKind kind;
    ssvm_word rel_addr;
    Scope *parent;
    List *compiled_loc_vars; // List : CompiledVar
};

typedef union {
    ssvm_word loc_var;  // Frame-relative address (accessible after sum with metadata)
    ssvm_word glob_var; // Absolute memory address (directly accessible)
} CompiledVarAs;

typedef struct {
    CompiledVarAs as;
    String name;
    Type type;
} CompiledVar;

typedef enum {
    REQ_NONE          = 1 << 0,
    REQ_PUSH_VAR_ADDR = 1 << 1,
} CompilerReq;

typedef struct {
    String name;
    size_t arity;
    Type ret_type;
    ssvm_word addr;
    List *arg_types; // List : Type (Ordered)
    bool is_special;
} CompiledFunDef;

extern bool error_hit;

Compiler *init_compiler();
void free_compiler(Compiler *comp);

CompiledFunDef *push_compiled_fundef(Compiler *comp, ssvm_cgen *cgen, String name, size_t arity, Type ret_type);
CompiledVar push_compile_time_var(Compiler *comp, ssvm_cgen *cgen, VarDef var_def, bool is_glob);
void push_local_var_addr(ssvm_cgen *cgen, CompiledVar var);
CompiledVar *search_local_var(Compiler *comp, String name);
CompiledVar *search_global_var(Compiler *comp, String name);

bool type_chk(Type t1, Type t2);
Type type_chk_expr(FileLoc loc, Type t1, Type t2);

void compile_program_to_file(Lexer *lexer, const char *fn, size_t mem_sz, bool dump_ast, bool run_prog);
void compile_tops_until_eof(Compiler *comp, ssvm_cgen *cgen, Lexer *lexer, bool dump_ast);
void compile_top(Compiler *comp, ssvm_cgen *cgen, Top top, bool dump_ast);
void compile_stmt(Compiler *comp, ssvm_cgen *cgen, Stmt stmt);
Type compile_expr(Compiler *comp, ssvm_cgen *cgen, Expr expr, CompilerReq req, size_t depth);

#endif  // _SCOMPILER_H
