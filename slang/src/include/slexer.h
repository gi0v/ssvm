#ifndef _SLEXER_H
#define _SLEXER_H

#include <strlib.h>
#include <fileloc.h>

typedef struct {
    String src;
    size_t last_cost;
    FileLoc glob_loc;
} Lexer;

typedef enum {
    TOK_FLAG_BIN_OP = 1 << 0,
    TOK_FLAG_RL     = 1 << 1,
} TokenFlags;

typedef enum {
    TOK_KIND_ID = 0,
    TOK_KIND_IF,
    TOK_KIND_AT,
    TOK_KIND_ADDSET,
    TOK_KIND_SUBSET,
    TOK_KIND_MULSET,
    TOK_KIND_DIVSET,
    TOK_KIND_REMSET,
    TOK_KIND_SHLSET,
    TOK_KIND_SHRSET,
    TOK_KIND_ANDSET,
    TOK_KIND_XORSET,
    TOK_KIND_ORSET,
    TOK_KIND_RET,
    TOK_KIND_LNOT,
    TOK_KIND_SET,
    TOK_KIND_CHR,
    TOK_KIND_STR,
    TOK_KIND_VAR,
    TOK_KIND_NUM,
    TOK_KIND_ADD,
    TOK_KIND_SUB,
    TOK_KIND_MUL,
    TOK_KIND_DIV,
    TOK_KIND_REM,
    TOK_KIND_SHL,
    TOK_KIND_SHR,
    TOK_KIND_GRE,
    TOK_KIND_GEQ,
    TOK_KIND_LES,
    TOK_KIND_LEQ,
    TOK_KIND_EQU,
    TOK_KIND_NEQ,
    TOK_KIND_AMP,
    TOK_KIND_BXOR,
    TOK_KIND_BOR,
    TOK_KIND_LAND,
    TOK_KIND_LOR,
    TOK_KIND_COL,
    TOK_KIND_FUN,
    TOK_KIND_LPAR,
    TOK_KIND_RPAR,
    TOK_KIND_LCUR,
    TOK_KIND_RCUR,
    TOK_KIND_LBRA,
    TOK_KIND_RBRA,
    TOK_KIND_SCOL,
    TOK_KIND_FOR,
    TOK_KIND_ELSE,
    TOK_KIND_WHILE,
    TOK_KIND_COMMA,
    TOK_KIND_BREAK,
    TOK_KIND_CONTINUE,
    TOK_KIND_EOF,
    TOK_COUNT,
} TokenKind;

typedef struct {
    String val;
    size_t cost;
    TokenKind kind;
    TokenFlags flags;
    FileLoc loc;
} Token;

Lexer *init_lexer(const char *fn);
void free_lexer(Lexer *l);
String chop_ln(Lexer *lexer);
Token lex_num(Lexer *lexer);
Token get_tok(Lexer *lexer);
void next_tok(Lexer *lexer);
Token expect_tok(Lexer *lexer, TokenKind k);
const char *tok_kind_to_cstr(TokenKind k);
void dump_tok(Token tok);
void free_tok(Token tok);

#endif  // _SLEXER_H
