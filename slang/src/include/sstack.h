#ifndef _SSTACK_H
#define _SSTACK_H

#include <scompiler.h>

#define CURR_FRAME_ADDR 0 + 0
#define STACK_SIZE_ADDR 0 + 8
#define GLOB_PREV_FRAME 0 + 8 + 8

#define PUSH_INSTR_WARG(cgen, instr, arg)                    \
    do {                                                     \
        push_instr(cgen, instr);                             \
        push_instr_arg(cgen, arg);                           \
    } while (false)                                          \

#define RD_CURR_FRAME(cgen)                                  \
    do {                                                     \
        PUSH_INSTR_WARG(cgen, INSTR_PUSH, CURR_FRAME_ADDR);  \
        push_instr(cgen, INSTR_RD64);                        \
    } while (false)                                          \

#define RD_STACK_SIZE(cgen)                                  \
    do {                                                     \
        PUSH_INSTR_WARG(cgen, INSTR_PUSH, STACK_SIZE_ADDR);  \
        push_instr(cgen, INSTR_RD64);                        \
    } while (false)                                          \

#define INC_STACK_SIZE(cgen, n)                              \
    do {                                                     \
        PUSH_INSTR_WARG(cgen, INSTR_PUSH, STACK_SIZE_ADDR);  \
        RD_STACK_SIZE(cgen);                                 \
        PUSH_INSTR_WARG(cgen, INSTR_PUSH, n);                \
        push_instr(cgen, INSTR_ADD);                         \
        push_instr(cgen, INSTR_WR64);                        \
    } while (false)                                          \

void push_scope(Compiler *comp, ScopeKind kind);
void pop_scope(Compiler *comp);
void free_single_scope(Scope *scope);
void free_all_scopes(Compiler *comp);
Scope *get_proc_scope(Compiler *comp);

void setup_call_stack(ssvm_cgen *cgen);
void push_frame(ssvm_cgen *cgen);
void pop_frame(ssvm_cgen *cgen);

#endif  // _SSTACK_H
