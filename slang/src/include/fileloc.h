#ifndef _FILELOC_H
#define _FILELOC_H

#include <stdbool.h>

#define FLFmt "%s:%zu:%zu"
#define FLNOLFmt "(%s, <row: %zu, col: %zu>)" // No-outline format
#define FLArg(fl) (fl).fn, (fl).y, (fl).x

#define INTERNAL_FL (FileLoc) { .x = 0, .y = 0, .fn = "stderr" }

typedef struct {
    size_t x, y;
    bool is_macro;
    const char *fn;
} FileLoc;

typedef enum {
    LOG_LVL_ERR,
    LOG_LVL_WARN,
    LOG_LVL_NOTE,
} LogLevel;

void log_msg(FileLoc loc, LogLevel lvl, const char *fmt, ...);

#define ASSERT(p, loc, ...)                             \
    do {                                                \
        if (!(p)) {                                     \
            log_msg(loc, LOG_LVL_ERR, ##__VA_ARGS__);   \
            exit(1);                                    \
        }                                               \
    } while (false)                                     \

#endif  // _FILELOC_H
