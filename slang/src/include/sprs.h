#ifndef _SPRS_H
#define _SPRS_H

#include <assert.h>
#include <stdint.h>

#include <list.h>
#include <ssvm.h>
#include <strlib.h>
#include <slexer.h>

typedef uint8_t Prec;

typedef enum {
    UNARY_OP_EXPR_KIND_ADDR_OF = 0,
    UNARY_OP_EXPR_KIND_DEREF,
    UNARY_OP_EXPRS_COUNT
} UnaryOpExprKind;

typedef enum {
    BIN_OP_EXPR_KIND_ADD = 0,
    BIN_OP_EXPR_KIND_SUB,
    BIN_OP_EXPR_KIND_MUL,
    BIN_OP_EXPR_KIND_DIV,
    BIN_OP_EXPR_KIND_REM,
    BIN_OP_EXPR_KIND_SHL,
    BIN_OP_EXPR_KIND_SHR,
    BIN_OP_EXPR_KIND_GRE,
    BIN_OP_EXPR_KIND_GEQ,
    BIN_OP_EXPR_KIND_LES,
    BIN_OP_EXPR_KIND_LEQ,
    BIN_OP_EXPR_KIND_EQU,
    BIN_OP_EXPR_KIND_NEQ,
    BIN_OP_EXPR_KIND_BAND,
    BIN_OP_EXPR_KIND_BXOR,
    BIN_OP_EXPR_KIND_BOR,
    BIN_OP_EXPR_KIND_LAND,
    BIN_OP_EXPR_KIND_LOR,
    BIN_OP_EXPR_KIND_SET,
    BIN_OP_EXPR_KIND_ADDSET,
    BIN_OP_EXPR_KIND_SUBSET,
    BIN_OP_EXPR_KIND_MULSET,
    BIN_OP_EXPR_KIND_DIVSET,
    BIN_OP_EXPR_KIND_REMSET,
    BIN_OP_EXPR_KIND_SHLSET,
    BIN_OP_EXPR_KIND_SHRSET,
    BIN_OP_EXPR_KIND_ANDSET,
    BIN_OP_EXPR_KIND_XORSET,
    BIN_OP_EXPR_KIND_ORSET,
    BIN_OP_EXPRS_COUNT
} BinOpExprKind;

typedef enum {
    EXPR_KIND_ID = 0,
    EXPR_KIND_NUM,
    EXPR_KIND_NOP,
    EXPR_KIND_STR,
    EXPR_KIND_INDEX,
    EXPR_KIND_BIN_OP,
    EXPR_KIND_UNARY_OP,
    EXPR_KIND_FUN_CALL,
    EXPRS_COUNT
} ExprKind;

typedef enum {
    STMT_KIND_EXPR = 0,
    STMT_KIND_IF,
    STMT_KIND_FOR,
    STMT_KIND_WHILE,
    STMT_KIND_VAR_DEF,
    STMT_KIND_COMPOUND,
    STMT_KIND_BREAK,
    STMT_KIND_CONTINUE,
    STMT_KIND_RETURN,
    STMTS_COUNT,
} StmtKind;

typedef enum {
    TOP_KIND_FUN_DEF = 0,
    TOP_KIND_VAR_DEF,
    TOP_KIND_INCLUDE,
    TOP_KIND_MACRO,
    TOPS_COUNT,
} TopKind;

typedef struct Expr Expr;
typedef struct IndexExpr IndexExpr;
typedef struct FunCallExpr FunCallExpr;
typedef struct BinOpExpr BinOpExpr;
typedef struct UnaryOpExpr UnaryOpExpr;

typedef struct Stmt Stmt;
typedef struct IfStmt IfStmt;
typedef struct ForLoop ForLoop;
typedef struct WhileLoop WhileLoop;
typedef struct VarDef VarDef;

typedef struct Top Top;
typedef struct FunDef FunDef;

static_assert(EXPRS_COUNT == 8,
              "Expressions count changed, "
              "please update the union below.");

typedef union {
    String id, str;
    uint64_t num;
    IndexExpr *index;
    BinOpExpr *bin_op;
    FunCallExpr *funcall;
    UnaryOpExpr *unary_op;
} ExprAs;

struct Expr {
    ExprAs as;
    FileLoc loc;
    ExprKind kind;
};

struct BinOpExpr {
    FileLoc loc;
    BinOpExprKind kind;
    Expr lhs, rhs;
};

struct UnaryOpExpr {
    Expr rhs;
    FileLoc loc;
    UnaryOpExprKind kind;
};

struct FunCallExpr {
    FileLoc loc;
    List *args; // List : Expr
    String name;
};

struct IndexExpr {
    Expr index;
    String id;
};

struct IfStmt {
    Expr cond;
    Stmt *body;
    Stmt *else_s;
    FileLoc loc;
};

struct ForLoop {
    Expr cond;
    Stmt *sstmt, *fstmt, *body;
    FileLoc loc;
};

struct WhileLoop {
    Expr cond;
    Stmt *body;
    FileLoc loc;
};

typedef enum {
    TYPE_KIND_U0 = 0,
    TYPE_KIND_U8,
    TYPE_KIND_U16,
    TYPE_KIND_U32,
    TYPE_KIND_U64,
    TYPE_KIND_U0_PTR,
    TYPE_KIND_U8_PTR,
    TYPE_KIND_U16_PTR,
    TYPE_KIND_U32_PTR,
    TYPE_KIND_U64_PTR,
    TYPES_COUNT,
} TypeKind;

typedef struct {
    size_t sz; // In bytes
    const char *name;
    TypeKind kind;

    struct {
        TypeKind ptr, non_ptr;

        struct { ssvm_instr_kind rd, wr; } instr;
    } as;
} Type;

struct VarDef {
    Type type;
    Expr var_expr;
    FileLoc loc;
    Expr init;
    bool has_init;
};

static_assert(STMTS_COUNT == 9,
              "Statements count changed, "
              "please update the union below.");

typedef union {
    Expr expr, ret;
    IfStmt if_stmt;
    ForLoop for_l;
    WhileLoop while_l;
    VarDef var_def;
    List *compound; // List : Stmt
} StmtAs;

struct Stmt {
    StmtAs as;
    FileLoc loc;
    StmtKind kind;
};

struct FunDef {
    FileLoc loc;
    String name;
    Stmt body;
    List *args; // List : VarDef
    Type ret_type;
};

static_assert(TOPS_COUNT == 4,
              "Top statements count has changed, "
              "please update the union below.");

typedef union {
    String include;
    FunDef fundef;
    VarDef var_def;
} TopAs;

struct Top {
    TopAs as;
    FileLoc loc;
    TopKind kind;
};

typedef struct {
    String src;
    String name;
    FileLoc loc;
} Macro;

extern List *macros;
extern Type type_defs[];

Type str_to_type(FileLoc loc, String s);

Top prs_top(Lexer *lexer);
Stmt prs_stmt(Lexer *lexer);
Expr prs_expr(Lexer *lexer);
Expr prs_primary(Lexer *lexer);

void free_expr(Expr expr);
void free_stmt(Stmt stmt);
void free_top(Top top);

void dump_expr(Expr expr);
void dump_stmt(Stmt stmt);
void dump_top(Top top);

#endif  // _SPRS_H
