#include <assert.h>

#include <sprs.h>
#include <slexer.h>

List *macros; // List : Macro

Macro *is_macro(String name)
{
    for (size_t i = 0; i < macros->items_count; ++i)
        if (STR_CMP(((Macro *)macros->items[i])->name, name))
            return macros->items[i];

    return NULL;
}

static_assert(TYPES_COUNT == 10, "Types count has changed, "
              "please update the array below.");

Type type_defs[] = {
    [TYPE_KIND_U0]      = { .sz = 0 << 0, .name = "u0",      .kind = TYPE_KIND_U0,      .as = { .ptr = TYPE_KIND_U0_PTR,  .non_ptr = TYPE_KIND_U0,  .instr = { .rd = INSTR_NOP,  .wr = INSTR_NOP  } } },
    [TYPE_KIND_U8]      = { .sz = 1 << 0, .name = "u8",      .kind = TYPE_KIND_U8,      .as = { .ptr = TYPE_KIND_U8_PTR,  .non_ptr = TYPE_KIND_U8,  .instr = { .rd = INSTR_RD8,  .wr = INSTR_WR8  } } },
    [TYPE_KIND_U16]     = { .sz = 1 << 1, .name = "u16",     .kind = TYPE_KIND_U16,     .as = { .ptr = TYPE_KIND_U16_PTR, .non_ptr = TYPE_KIND_U16, .instr = { .rd = INSTR_RD16, .wr = INSTR_WR16 } } },
    [TYPE_KIND_U32]     = { .sz = 1 << 2, .name = "u32",     .kind = TYPE_KIND_U32,     .as = { .ptr = TYPE_KIND_U32_PTR, .non_ptr = TYPE_KIND_U32, .instr = { .rd = INSTR_RD32, .wr = INSTR_WR32 } } },
    [TYPE_KIND_U64]     = { .sz = 1 << 3, .name = "u64",     .kind = TYPE_KIND_U64,     .as = { .ptr = TYPE_KIND_U64_PTR, .non_ptr = TYPE_KIND_U64, .instr = { .rd = INSTR_RD64, .wr = INSTR_WR64 } } },
    [TYPE_KIND_U0_PTR]  = { .sz = 1 << 3, .name = "u0_ptr",  .kind = TYPE_KIND_U0_PTR,  .as = { .ptr = TYPE_KIND_U0_PTR,  .non_ptr = TYPE_KIND_U0,  .instr = { .rd = INSTR_RD64, .wr = INSTR_WR64 } } },
    [TYPE_KIND_U8_PTR]  = { .sz = 1 << 3, .name = "u8_ptr",  .kind = TYPE_KIND_U8_PTR,  .as = { .ptr = TYPE_KIND_U8_PTR,  .non_ptr = TYPE_KIND_U8,  .instr = { .rd = INSTR_RD64, .wr = INSTR_WR64 } } },
    [TYPE_KIND_U16_PTR] = { .sz = 1 << 3, .name = "u16_ptr", .kind = TYPE_KIND_U16_PTR, .as = { .ptr = TYPE_KIND_U16_PTR, .non_ptr = TYPE_KIND_U16, .instr = { .rd = INSTR_RD64, .wr = INSTR_WR64 } } },
    [TYPE_KIND_U32_PTR] = { .sz = 1 << 3, .name = "u32_ptr", .kind = TYPE_KIND_U32_PTR, .as = { .ptr = TYPE_KIND_U32_PTR, .non_ptr = TYPE_KIND_U32, .instr = { .rd = INSTR_RD64, .wr = INSTR_WR64 } } },
    [TYPE_KIND_U64_PTR] = { .sz = 1 << 3, .name = "u64_ptr", .kind = TYPE_KIND_U64_PTR, .as = { .ptr = TYPE_KIND_U64_PTR, .non_ptr = TYPE_KIND_U64, .instr = { .rd = INSTR_RD64, .wr = INSTR_WR64 } } },
};

Type str_to_type(FileLoc loc, String s)
{
    for (size_t i = 0; i < TYPES_COUNT; ++i)
        if (STR_CSTR_CMP(s, type_defs[i].name))
            return type_defs[i];

    log_msg(loc, LOG_LVL_ERR, "unknown type name `"STRFmt"`.", STRArg(s));
    exit(1);
}

static_assert(BIN_OP_EXPRS_COUNT == 29,
              "Binary operations count changed, "
              "please update (1) below.");

// -- Start (1)

Prec tok_to_prec(Token t)
{
    switch (t.kind) {
    case TOK_KIND_SET: case TOK_KIND_ADDSET: case TOK_KIND_SUBSET: case TOK_KIND_MULSET: case TOK_KIND_DIVSET: case TOK_KIND_REMSET: case TOK_KIND_SHLSET: case TOK_KIND_SHRSET: case TOK_KIND_ANDSET: case TOK_KIND_XORSET: case TOK_KIND_ORSET: return 1;
    case TOK_KIND_LOR:                                                          return 2;
    case TOK_KIND_LAND:                                                         return 3;
    case TOK_KIND_BOR:                                                          return 4;
    case TOK_KIND_BXOR:                                                         return 5;
    case TOK_KIND_AMP:                                                          return 6;
    case TOK_KIND_EQU: case TOK_KIND_NEQ:                                       return 7;
    case TOK_KIND_LES: case TOK_KIND_LEQ: case TOK_KIND_GRE: case TOK_KIND_GEQ: return 8;
    case TOK_KIND_SHL: case TOK_KIND_SHR:                                       return 9;
    case TOK_KIND_ADD: case TOK_KIND_SUB:                                       return 10;
    case TOK_KIND_MUL: case TOK_KIND_DIV: case TOK_KIND_REM:                    return 11;
    default:                                                                    return 0;
    }
}

BinOpExprKind tok_bin_op_to_expr[] = {
    [TOK_KIND_ADD] = BIN_OP_EXPR_KIND_ADD,
    [TOK_KIND_SUB] = BIN_OP_EXPR_KIND_SUB,
    [TOK_KIND_MUL] = BIN_OP_EXPR_KIND_MUL,
    [TOK_KIND_DIV] = BIN_OP_EXPR_KIND_DIV,
    [TOK_KIND_REM] = BIN_OP_EXPR_KIND_REM,
    [TOK_KIND_LOR] = BIN_OP_EXPR_KIND_LOR,
    [TOK_KIND_LAND] = BIN_OP_EXPR_KIND_LAND,
    [TOK_KIND_BOR] = BIN_OP_EXPR_KIND_BOR,
    [TOK_KIND_BXOR] = BIN_OP_EXPR_KIND_BXOR,
    [TOK_KIND_AMP] = BIN_OP_EXPR_KIND_BAND,
    [TOK_KIND_EQU] = BIN_OP_EXPR_KIND_EQU,
    [TOK_KIND_NEQ] = BIN_OP_EXPR_KIND_NEQ,
    [TOK_KIND_LES] = BIN_OP_EXPR_KIND_LES,
    [TOK_KIND_LEQ] = BIN_OP_EXPR_KIND_LEQ,
    [TOK_KIND_GRE] = BIN_OP_EXPR_KIND_GRE,
    [TOK_KIND_GEQ] = BIN_OP_EXPR_KIND_GEQ,
    [TOK_KIND_SHL] = BIN_OP_EXPR_KIND_SHL,
    [TOK_KIND_SHR] = BIN_OP_EXPR_KIND_SHR,
    [TOK_KIND_SET] = BIN_OP_EXPR_KIND_SET,
    [TOK_KIND_ADDSET] = BIN_OP_EXPR_KIND_ADDSET,
    [TOK_KIND_SUBSET] = BIN_OP_EXPR_KIND_SUBSET,
    [TOK_KIND_MULSET] = BIN_OP_EXPR_KIND_MULSET,
    [TOK_KIND_DIVSET] = BIN_OP_EXPR_KIND_DIVSET,
    [TOK_KIND_REMSET] = BIN_OP_EXPR_KIND_REMSET,
    [TOK_KIND_SHLSET] = BIN_OP_EXPR_KIND_SHLSET,
    [TOK_KIND_SHRSET] = BIN_OP_EXPR_KIND_SHRSET,
    [TOK_KIND_ANDSET] = BIN_OP_EXPR_KIND_ANDSET,
    [TOK_KIND_XORSET] = BIN_OP_EXPR_KIND_XORSET,
    [TOK_KIND_ORSET] = BIN_OP_EXPR_KIND_ORSET,
};

// -- End   (1)

VarDef prs_var_def(Lexer *lexer)
{
    Token tok = {0};
    VarDef var_def = {0};

    var_def.var_expr = prs_primary(lexer);
    var_def.loc = var_def.var_expr.loc;
    
    free_tok(expect_tok(lexer, TOK_KIND_COL));
    
    tok = expect_tok(lexer, TOK_KIND_ID);

    var_def.type = str_to_type(tok.loc, tok.val);

    free_tok(tok);
    tok = get_tok(lexer);

    if (tok.kind == TOK_KIND_MUL) {
        var_def.type = type_defs[var_def.type.as.ptr];
        next_tok(lexer);
    }

    free_tok(tok);
    tok = get_tok(lexer);

    if (tok.kind == TOK_KIND_SET) {
        next_tok(lexer);

        var_def.init = prs_expr(lexer);
        var_def.has_init = true;
    } else var_def.has_init = false;

    free_tok(tok);

    return var_def;
}

// NOTE: This might be used for structs
// in a syntax like:
//   struct MyStruct (
//       a: u8,
//       b: u8
//   );
List *prs_args_list(Lexer *lexer)
{
    List *l = init_list(sizeof(VarDef *));
    Token tok = {0};
    VarDef *arg = NULL;

    free_tok(expect_tok(lexer, TOK_KIND_LPAR));

    tok = get_tok(lexer);

    if (tok.kind != TOK_KIND_RPAR) {
        arg = calloc(1, sizeof(*arg));
        *arg = prs_var_def(lexer);
        add_to_list(l, arg);

        free_tok(tok);
        tok = get_tok(lexer);

        while (tok.kind == TOK_KIND_COMMA) {
            next_tok(lexer);

            arg = calloc(1, sizeof(*arg));
            *arg = prs_var_def(lexer);
            add_to_list(l, arg);
            
            free_tok(tok);
            tok = get_tok(lexer);
        }
    } free_tok(expect_tok(lexer, TOK_KIND_RPAR));
    
    free_tok(tok);

    return l;
}

static_assert(TOPS_COUNT == 4,
              "Top statements count has changed, "
              "please update the function below.");

Top prs_top(Lexer *lexer)
{
    Top top = {0};
    Token tok = get_tok(lexer);

    top.loc = tok.loc;

    switch (tok.kind) {

    case TOK_KIND_FUN: {
        // Function definition
        
        free_tok(tok);
        next_tok(lexer);
        
        tok = expect_tok(lexer, TOK_KIND_ID);
        
        top.kind = TOP_KIND_FUN_DEF;
        top.as.fundef.loc = top.loc;
        top.as.fundef.args = prs_args_list(lexer);
        top.as.fundef.name = str_from_static_cstr(STR_CSTR(tok.val));

        free_tok(tok);
        free_tok(expect_tok(lexer, TOK_KIND_COL));
        tok = expect_tok(lexer, TOK_KIND_ID);

        top.as.fundef.ret_type = str_to_type(tok.loc, tok.val);
        top.as.fundef.body = prs_stmt(lexer);
    } break;

    case TOK_KIND_VAR: {
        // Global variable definition

        next_tok(lexer);

        top.kind = TOP_KIND_VAR_DEF;
        top.as.var_def = prs_var_def(lexer);

        free_tok(expect_tok(lexer, TOK_KIND_SCOL));
    } break;

    case TOK_KIND_AT: {
        // Preprocessor (not really) directive

        free_tok(tok);
        next_tok(lexer);

        tok = expect_tok(lexer, TOK_KIND_ID);

        if (STR_CSTR_CMP(tok.val, "include")) {
            top.kind = TOP_KIND_INCLUDE;

            free_tok(tok);
            tok = expect_tok(lexer, TOK_KIND_STR);
            top.as.include = str_from_static_cstr(STR_CSTR(tok.val));
        } else if (STR_CSTR_CMP(tok.val, "macro")) {
            // Arguments are not supported for now

            free_tok(tok);
            tok = expect_tok(lexer, TOK_KIND_ID);

            top.kind = TOP_KIND_MACRO;

            Macro *macro = calloc(1, sizeof(*macro));
            {
                macro->loc = top.loc;
                macro->loc.is_macro = true;
                macro->name = str_from_static_cstr(STR_CSTR(tok.val));
                macro->src = chop_ln(lexer);
            }
            add_to_list(macros, macro);
        } else log_msg(top.loc, LOG_LVL_ERR, "unknown preprocessor directive `"STRFmt"`", STRArg(tok.val));
    } break;

    case TOK_KIND_ID: {
        Macro *macro = is_macro(tok.val);

        if (macro != NULL) {
            next_tok(lexer);

            Lexer *l2 = calloc(1, sizeof(*l2));
            {
                l2->src = str_from_static_cstr(STR_CSTR(macro->src));
                l2->src = str_append(l2->src, STR_CSTR(lexer->src));
                l2->last_cost = 0;
                l2->glob_loc = macro->loc;
            }
            
            top = prs_top(l2);

            str_free(lexer->src);
            lexer->src = l2->src;
            
            free(l2);
        } else {
            log_msg(tok.loc, LOG_LVL_ERR, "invalid token `"STRFmt"` for top statement.",
                    STRArg(tok.val));
            exit(1);
        }
    } break;

    default:
        log_msg(tok.loc, LOG_LVL_ERR, "invalid token `"STRFmt"` for top statement.",
                STRArg(tok.val));
        exit(1);
    }

    free_tok(tok);

    return top;
}

static_assert(STMTS_COUNT == 9,
              "Statements count changed, "
              "please update the function below.");

Stmt prs_stmt(Lexer *lexer)
{
    Stmt stmt = {0};
    Token tok = get_tok(lexer);

    stmt.loc = tok.loc;

    switch (tok.kind) {

    case TOK_KIND_IF: {
        // If statement

        free_tok(tok);
        next_tok(lexer);

        stmt.kind = STMT_KIND_IF;
        stmt.as.if_stmt.cond = prs_expr(lexer);
        stmt.as.if_stmt.body = calloc(1, sizeof(Stmt));
        {
            *stmt.as.if_stmt.body = prs_stmt(lexer);
        }
        stmt.as.if_stmt.else_s = NULL;

        tok = get_tok(lexer);
        if (tok.kind == TOK_KIND_ELSE) {
            next_tok(lexer);
            stmt.as.if_stmt.else_s = calloc(1, sizeof(Stmt));
            {
                *stmt.as.if_stmt.else_s = prs_stmt(lexer);
            }
        }
    } break;

    case TOK_KIND_WHILE: {
        // While loop

        next_tok(lexer);

        stmt.kind = STMT_KIND_WHILE;
        stmt.as.while_l.loc = stmt.loc;
        stmt.as.while_l.cond = prs_expr(lexer);
        stmt.as.while_l.body = calloc(1, sizeof(Stmt));
        {
            *stmt.as.while_l.body = prs_stmt(lexer);
        }
    } break;

    case TOK_KIND_FOR: {
        // For loop

        next_tok(lexer);

        stmt.kind = STMT_KIND_FOR;
        stmt.as.for_l.loc = stmt.loc;
        stmt.as.for_l.sstmt = calloc(1, sizeof(Stmt));
        {
            *stmt.as.for_l.sstmt = prs_stmt(lexer);
        }
        stmt.as.for_l.cond = prs_expr(lexer);
        free_tok(expect_tok(lexer, TOK_KIND_SCOL));
        stmt.as.for_l.fstmt = calloc(1, sizeof(Stmt));
        {
            *stmt.as.for_l.fstmt = prs_stmt(lexer);
        }
        stmt.as.for_l.body = calloc(1, sizeof(Stmt));
        {
            *stmt.as.for_l.body = prs_stmt(lexer);
        }
    } break;

    case TOK_KIND_RET: {
        next_tok(lexer);

        stmt.kind = STMT_KIND_RETURN;
        stmt.as.ret = prs_expr(lexer);

        if (stmt.as.ret.kind != EXPR_KIND_NOP)
            free_tok(expect_tok(lexer, TOK_KIND_SCOL));
    } break;

    case TOK_KIND_VAR: {
        // Local variable definition

        next_tok(lexer);

        stmt.kind = STMT_KIND_VAR_DEF;
        stmt.as.var_def = prs_var_def(lexer);

        free_tok(expect_tok(lexer, TOK_KIND_SCOL));
    } break;

    case TOK_KIND_LCUR: {
        // Compound statement

        Stmt *ps = NULL;

        free_tok(tok);
        next_tok(lexer);

        stmt.kind = STMT_KIND_COMPOUND;
        stmt.as.compound = init_list(sizeof(Stmt *));

        tok = get_tok(lexer);

        while (tok.kind != TOK_KIND_RCUR) {
            ps = calloc(1, sizeof(Stmt));
            {
                *ps = prs_stmt(lexer);
            }
            add_to_list(stmt.as.compound, ps);

            free_tok(tok);
            tok = get_tok(lexer);
        } free_tok(expect_tok(lexer, TOK_KIND_RCUR));
    } break;

    case TOK_KIND_BREAK: {
        next_tok(lexer);
        stmt.kind = STMT_KIND_BREAK;
        free_tok(expect_tok(lexer, TOK_KIND_SCOL));
    } break;

    case TOK_KIND_CONTINUE: {
        next_tok(lexer);
        stmt.kind = STMT_KIND_CONTINUE;
        free_tok(expect_tok(lexer, TOK_KIND_SCOL));
    } break;

    case TOK_KIND_ID: {
        Macro *macro = is_macro(tok.val);

        if (macro != NULL) {
            next_tok(lexer);

            Lexer *l2 = calloc(1, sizeof(*l2));
            {
                l2->src = str_from_static_cstr(STR_CSTR(macro->src));
                l2->src = str_append(l2->src, STR_CSTR(lexer->src));
                l2->last_cost = 0;
                l2->glob_loc = macro->loc;
            }
            
            stmt = prs_stmt(l2);

            str_free(lexer->src);
            lexer->src = l2->src;
            
            free(l2);
        } else {
            stmt.kind = STMT_KIND_EXPR;
            stmt.as.expr = prs_expr(lexer);
            
            free_tok(expect_tok(lexer, TOK_KIND_SCOL));
        }
    } break;

    default:
        stmt.kind = STMT_KIND_EXPR;
        stmt.as.expr = prs_expr(lexer);

        free_tok(expect_tok(lexer, TOK_KIND_SCOL));
    }

    free_tok(tok);

    return stmt;
}

Expr prs_expr_prec(Lexer *lexer, Expr lhs, Prec p)
{
    Expr rhs = {0};
    Token op = {0};
    Token lah = get_tok(lexer);
    FileLoc loc = lhs.loc;
    Prec op_prec, lah_prec;

    while ((lah.flags & TOK_FLAG_BIN_OP) && (tok_to_prec(lah) >= p)) {
        next_tok(lexer);

        op = lah;
        rhs = prs_primary(lexer);
        lah = get_tok(lexer);

        op_prec = tok_to_prec(op);
        lah_prec = tok_to_prec(lah);

        while ((lah.flags & TOK_FLAG_BIN_OP) && ((lah_prec > op_prec)
                                                 || (lah_prec == op_prec && lah.flags & TOK_FLAG_RL))) {
            rhs = prs_expr_prec(lexer, rhs, op_prec + ((lah_prec > op_prec) ? 1 : 0));
            free_tok(lah);
            lah = get_tok(lexer);
            lah_prec = tok_to_prec(lah);
        }

        BinOpExpr *tmp2 = calloc(1, sizeof(*tmp2));
        {
            tmp2->loc = loc;
            tmp2->kind = tok_bin_op_to_expr[op.kind];
            tmp2->lhs = lhs;
            tmp2->rhs = rhs;
        }
        lhs.loc = loc;
        lhs.kind = EXPR_KIND_BIN_OP;
        lhs.as.bin_op = tmp2;

        free_tok(op);
    }

    free_tok(lah);

    return lhs;
}

Expr prs_expr(Lexer *lexer)
{
    return prs_expr_prec(lexer, prs_primary(lexer), 0);
}

static_assert(UNARY_OP_EXPRS_COUNT == 2, "Unary operations count changed, "
              "please update the function below.");

Expr prs_primary(Lexer *lexer)
{
    Expr expr = {0};
    Token tok = get_tok(lexer);

    next_tok(lexer);

    expr.loc = tok.loc;

    switch (tok.kind) {

        // Rule note: unary expressions have all the maximum precedence.
    case TOK_KIND_AMP: {
        expr.kind = EXPR_KIND_UNARY_OP;
        expr.as.unary_op = calloc(1, sizeof(UnaryOpExpr));
        {
            expr.as.unary_op->kind = UNARY_OP_EXPR_KIND_ADDR_OF;
            expr.as.unary_op->loc = expr.loc;
            expr.as.unary_op->rhs = prs_primary(lexer);
        }
    } break;

    case TOK_KIND_MUL: {
        expr.kind = EXPR_KIND_UNARY_OP;
        expr.as.unary_op = calloc(1, sizeof(UnaryOpExpr));
        {
            expr.as.unary_op->kind = UNARY_OP_EXPR_KIND_DEREF;
            expr.as.unary_op->loc = expr.loc;
            expr.as.unary_op->rhs = prs_primary(lexer);
        }
    } break;

    case TOK_KIND_ID: {
        Macro *macro = is_macro(tok.val);
        if (macro != NULL) {
            Lexer *l2 = calloc(1, sizeof(*l2));
            {
                l2->src = str_from_static_cstr(STR_CSTR(macro->src));
                l2->last_cost = 0;
                l2->glob_loc = macro->loc;
            }

            expr = prs_expr(l2);

            str_free(l2->src);
            free(l2);
        } else {
            Token peek = get_tok(lexer);
            
            if (peek.kind == TOK_KIND_LPAR) {
                // Function call
                
                free_tok(peek);
                next_tok(lexer);
                
                FunCallExpr *funcall = calloc(1, sizeof(*funcall));
                {
                    funcall->name = str_from_static_cstr(STR_CSTR(tok.val));
                    funcall->args = init_list(sizeof(Expr));
                    funcall->loc = tok.loc;
                }
                
                peek = get_tok(lexer);
                
                if (peek.kind != TOK_KIND_RPAR) {
                    Expr *pe = NULL;
                    
                    free_tok(peek);
                    
                    pe = calloc(1, sizeof(Expr));
                    {
                        *pe = prs_expr(lexer);
                    }
                    add_to_list(funcall->args, pe);
                    
                    peek = get_tok(lexer);
                    
                    while (peek.kind == TOK_KIND_COMMA) {
                        free_tok(peek);
                        next_tok(lexer);
                        
                        pe = calloc(1, sizeof(Expr));
                        {
                            *pe = prs_expr(lexer);
                        }
                        add_to_list(funcall->args, pe);
                        
                        peek = get_tok(lexer);
                    }
                } free_tok(expect_tok(lexer, TOK_KIND_RPAR));
                
                expr.kind = EXPR_KIND_FUN_CALL;
                expr.as.funcall = funcall;
            } else if (peek.kind == TOK_KIND_LBRA) {
                // Indexing op.
                
                next_tok(lexer);
                
                expr.kind = EXPR_KIND_INDEX;
                expr.as.index = calloc(1, sizeof(IndexExpr));
                {
                    expr.as.index->index = prs_expr(lexer);
                    expr.as.index->id = str_from_static_cstr(STR_CSTR(tok.val));
                }
                
                free_tok(expect_tok(lexer, TOK_KIND_RBRA));
            } else {
                // ID (variable read)
                
                expr.kind = EXPR_KIND_ID;
                expr.as.id = str_from_static_cstr(STR_CSTR(tok.val));
            }
            
            free_tok(peek); 
        }
    } break;

    case TOK_KIND_NUM:
        expr.kind = EXPR_KIND_NUM;
        // TODO: Make also hex (or even binary) compatible by
        // checking for 0x- (or 0b-) prefix.
        expr.as.num = strtoull(STR_CSTR(tok.val), NULL, 10);
        break;
    case TOK_KIND_CHR:
        expr.kind = EXPR_KIND_NUM;
        expr.as.num = str_get_char(tok.val, 0);
        break;
    case TOK_KIND_STR:
        expr.kind = EXPR_KIND_STR;
        expr.as.str = str_from_static_cstr(STR_CSTR(tok.val));
        break;
    case TOK_KIND_LPAR:
        expr = prs_expr(lexer);
        free_tok(expect_tok(lexer, TOK_KIND_RPAR));
        break;
    case TOK_KIND_SCOL:
        expr.kind = EXPR_KIND_NOP;
        break;
    default:
        log_msg(tok.loc, LOG_LVL_ERR,
                "unexpected token `"STRFmt"`.", STRArg(tok.val));
        exit(1);
    }

    free_tok(tok);

    return expr;
}

static_assert(EXPRS_COUNT == 8, "Expression count changed, "
              "please update the function below.");

void free_expr(Expr expr)
{
    switch (expr.kind) {
    case EXPR_KIND_ID:
        str_free(expr.as.id);
        break;
    case EXPR_KIND_STR:
        str_free(expr.as.str);
        break;
    case EXPR_KIND_INDEX:
        str_free(expr.as.index->id);
        free_expr(expr.as.index->index);
        free(expr.as.index);
        break;
    case EXPR_KIND_BIN_OP:
        free_expr(expr.as.bin_op->lhs);
        free_expr(expr.as.bin_op->rhs);
        free(expr.as.bin_op);
        break;
    case EXPR_KIND_UNARY_OP:
        free_expr(expr.as.unary_op->rhs);
        free(expr.as.unary_op);
        break;
    case EXPR_KIND_FUN_CALL:
        str_free(expr.as.funcall->name);

        for (size_t i = 0; i < expr.as.funcall->args->items_count; ++i) {
            free_expr(*((Expr *) expr.as.funcall->args->items[i]));
            free(expr.as.funcall->args->items[i]);
        }

        free_list(expr.as.funcall->args);
        free(expr.as.funcall);

        break;
    default: return;
    }
}

static_assert(STMTS_COUNT == 9,
              "Statements count changed, "
              "please update the function below.");

void free_stmt(Stmt stmt)
{
    switch (stmt.kind) {
    case STMT_KIND_VAR_DEF:
        free_expr(stmt.as.var_def.var_expr);

        if (stmt.as.var_def.has_init)
            free_expr(stmt.as.var_def.init);
        break;
    case STMT_KIND_IF:
        free_expr(stmt.as.if_stmt.cond);
        free_stmt(*stmt.as.if_stmt.body);
        free(stmt.as.if_stmt.body);

        if (stmt.as.if_stmt.else_s != NULL) {
            free_stmt(*stmt.as.if_stmt.else_s);
            free(stmt.as.if_stmt.else_s);
        }
        break;
    case STMT_KIND_FOR:
        free_expr(stmt.as.for_l.cond);
        free_stmt(*stmt.as.for_l.sstmt);
        free_stmt(*stmt.as.for_l.fstmt);
        free_stmt(*stmt.as.for_l.body);
        free(stmt.as.for_l.sstmt);
        free(stmt.as.for_l.fstmt);
        free(stmt.as.for_l.body);
        break;
    case STMT_KIND_WHILE:
        free_expr(stmt.as.while_l.cond);
        free_stmt(*stmt.as.while_l.body);
        free(stmt.as.while_l.body);
        break;
    case STMT_KIND_EXPR:
        free_expr(stmt.as.expr);
        break;
    case STMT_KIND_RETURN:
        free_expr(stmt.as.ret);
        break;
    case STMT_KIND_COMPOUND:
        for (size_t i = 0; i < stmt.as.compound->items_count; ++i) {
            free_stmt(*((Stmt *)stmt.as.compound->items[i]));
            free(stmt.as.compound->items[i]);
        }

        free_list(stmt.as.compound);
        break;
    case STMT_KIND_BREAK:
    case STMT_KIND_CONTINUE:
        break;
    default: assert(false && "Unreachable.");
    }
}

static_assert(TOPS_COUNT == 4,
              "Top statements count has changed, "
              "please update the function below.");

void free_top(Top top)
{
    switch (top.kind) {
    case TOP_KIND_FUN_DEF:
        str_free(top.as.fundef.name);
        free_stmt(top.as.fundef.body);

        for (size_t i = 0; i < top.as.fundef.args->items_count; ++i) {
            free_expr(((VarDef *)top.as.fundef.args->items[i])->var_expr);
            free(top.as.fundef.args->items[i]);
        } free_list(top.as.fundef.args);
        break;
    case TOP_KIND_VAR_DEF:
        free_expr(top.as.var_def.var_expr);

        if (top.as.var_def.has_init)
            free_expr(top.as.var_def.init);
        break;
    case TOP_KIND_INCLUDE:
        str_free(top.as.include);
        break;
    case TOP_KIND_MACRO:
        return;
    default:
        assert(false && "Unreachable.");
    }
}

void dump_depth(size_t depth)
{
  if (depth == 0) {
    printf("+- ");
    return;
  }

  for (size_t i = 0; i < depth; ++i)
    printf("|  ");

  printf("`- ");
}

static_assert(BIN_OP_EXPRS_COUNT == 29,
              "Binary operations count changed, "
              "please update the function below.");

const char *bin_op_expr_kind_to_cstr(BinOpExprKind k)
{
    switch (k) {
    case BIN_OP_EXPR_KIND_ADD:     return "BinOpExprKindAdd";
    case BIN_OP_EXPR_KIND_SUB:     return "BinOpExprKindSub";
    case BIN_OP_EXPR_KIND_MUL:     return "BinOpExprKindMul";
    case BIN_OP_EXPR_KIND_DIV:     return "BinOpExprKindDiv";
    case BIN_OP_EXPR_KIND_REM:     return "BinOpExprKindRem";
    case BIN_OP_EXPR_KIND_SHL:     return "BinOpExprKindShl";
    case BIN_OP_EXPR_KIND_SHR:     return "BinOpExprKindShr";
    case BIN_OP_EXPR_KIND_GRE:     return "BinOpExprKindGre";
    case BIN_OP_EXPR_KIND_GEQ:     return "BinOpExprKindGeq";
    case BIN_OP_EXPR_KIND_LES:     return "BinOpExprKindLes";
    case BIN_OP_EXPR_KIND_LEQ:     return "BinOpExprKindLeq";
    case BIN_OP_EXPR_KIND_EQU:     return "BinOpExprKindEqu";
    case BIN_OP_EXPR_KIND_NEQ:     return "BinOpExprKindNeq";
    case BIN_OP_EXPR_KIND_BAND:    return "BinOpExprKindBAnd";
    case BIN_OP_EXPR_KIND_BXOR:    return "BinOpExprKindBXor";
    case BIN_OP_EXPR_KIND_BOR:     return "BinOpExprKindBOr";
    case BIN_OP_EXPR_KIND_LAND:    return "BinOpExprKindLAnd";
    case BIN_OP_EXPR_KIND_LOR:     return "BinOpExprKindLOr";
    case BIN_OP_EXPR_KIND_SET:     return "BinOpExprKindSet";
    case BIN_OP_EXPR_KIND_ADDSET:  return "BinOpExprKindAddSet";
    case BIN_OP_EXPR_KIND_SUBSET:  return "BinOpExprKindSubSet";
    case BIN_OP_EXPR_KIND_MULSET:  return "BinOpExprKindMulSet";
    case BIN_OP_EXPR_KIND_DIVSET:  return "BinOpExprKindDivSet";
    case BIN_OP_EXPR_KIND_REMSET:  return "BinOpExprKindRemSet";
    case BIN_OP_EXPR_KIND_SHLSET:  return "BinOpExprKindShlSet";
    case BIN_OP_EXPR_KIND_SHRSET:  return "BinOpExprKindShrSet";
    case BIN_OP_EXPR_KIND_ANDSET:  return "BinOpExprKindAndSet";
    case BIN_OP_EXPR_KIND_XORSET:  return "BinOpExprKindXorSet";
    case BIN_OP_EXPR_KIND_ORSET:   return "BinOpExprKindOrSet";
    default: return NULL;
    }
}

static_assert(UNARY_OP_EXPRS_COUNT == 2, "Unary operations count changed, "
              "please update the function below.");

const char *unary_op_expr_kind_to_cstr(UnaryOpExprKind k)
{
    switch (k) {
    case UNARY_OP_EXPR_KIND_ADDR_OF: return "UnaryOpExprKindAddrOf";
    case UNARY_OP_EXPR_KIND_DEREF:   return "UnaryOpExprKindDeref";
    default: return NULL;
    }
}

static_assert(EXPRS_COUNT == 8, "Expression count changed, "
              "please update the function below.");

void dump_expr_depth(Expr expr, size_t depth)
{
    dump_depth(depth);

    switch (expr.kind) {
    case EXPR_KIND_NOP:
        printf("ExprKindNop\n");
        break;
    case EXPR_KIND_ID:
        printf("ExprKindID ("STRFmt") "FLNOLFmt"\n",
               STRArg(expr.as.id), FLArg(expr.loc));
        break;
    case EXPR_KIND_INDEX:
        printf("ExprKindIndex ("STRFmt") "FLNOLFmt"\n",
               STRArg(expr.as.index->id), FLArg(expr.loc));

        dump_depth(depth + 1);
        printf("ExprKindIndexIndex\n");

        dump_expr_depth(expr.as.index->index, depth + 2);
        break;
    case EXPR_KIND_STR:
        printf("ExprKindStr (\""STRFmt"\") "FLNOLFmt"\n",
               STRArg(expr.as.str), FLArg(expr.loc));
        break;
    case EXPR_KIND_NUM:
        printf("ExprKindNum (%lu) "FLNOLFmt"\n", expr.as.num, FLArg(expr.loc));
        break;
    case EXPR_KIND_BIN_OP:
        printf("%s "FLNOLFmt"\n",
               bin_op_expr_kind_to_cstr(expr.as.bin_op->kind),
               FLArg(expr.as.bin_op->loc));

        dump_expr_depth(expr.as.bin_op->lhs, depth + 1);
        dump_expr_depth(expr.as.bin_op->rhs, depth + 1);
        break;
    case EXPR_KIND_UNARY_OP:
        printf("%s "FLNOLFmt"\n",
               unary_op_expr_kind_to_cstr(expr.as.unary_op->kind),
               FLArg(expr.as.unary_op->loc));

        dump_expr_depth(expr.as.unary_op->rhs, depth + 1);
        break;
    case EXPR_KIND_FUN_CALL:
        printf("ExprKindFunCall ("STRFmt") "FLNOLFmt"\n",
               STRArg(expr.as.funcall->name),
               FLArg(expr.as.funcall->loc));

        dump_depth(depth + 1);
        printf("ExprKindFunArgs\n");

        for (size_t i = 0; i < expr.as.funcall->args->items_count; ++i)
            dump_expr_depth(*((Expr *) expr.as.funcall->args->items[i]), depth + 2);

        break;
    default:
        assert(false && "Unreachable.");
    }
}

void dump_expr(Expr expr)
{
    dump_expr_depth(expr, 0);
}

static_assert(STMTS_COUNT == 9,
              "Statements count changed, "
              "please update the function below.");

void dump_stmt_depth(Stmt stmt, size_t depth)
{
    dump_depth(depth);

    switch (stmt.kind) {
    case STMT_KIND_VAR_DEF:
        printf("StmtKindVarDef (type: %s) "FLNOLFmt"\n",
               stmt.as.var_def.type.name, FLArg(stmt.loc));

        dump_depth(depth + 1);
        printf("StmtKindVarDefExpr\n");
        dump_expr_depth(stmt.as.var_def.var_expr, depth + 2);

        if (stmt.as.var_def.has_init) {
            dump_depth(depth + 1);
            printf("StmtKindVarDefInit\n");
            dump_expr_depth(stmt.as.var_def.init, depth + 2);
        }
        break;
    case STMT_KIND_IF:
        printf("StmtKindIf "FLNOLFmt"\n", FLArg(stmt.loc));

        dump_depth(depth + 1);
        printf("StmtKindIfCondition\n");
        dump_expr_depth(stmt.as.if_stmt.cond, depth + 2);

        dump_depth(depth + 1);
        printf("StmtKindIfBody\n");
        dump_stmt_depth(*stmt.as.if_stmt.body, depth + 2);

        if (stmt.as.if_stmt.else_s != NULL) {
            dump_depth(depth + 1);
            printf("StmtKindElse\n");
            dump_stmt_depth(*stmt.as.if_stmt.else_s, depth + 2);
        }
        break;
    case STMT_KIND_FOR:
        printf("StmtKindFor "FLNOLFmt"\n", FLArg(stmt.loc));

        dump_depth(depth + 1);
        printf("StmtKindForStartingStmt\n");
        dump_stmt_depth(*stmt.as.for_l.sstmt, depth + 2);

        dump_depth(depth + 1);
        printf("StmtKindForCondition\n");
        dump_expr_depth(stmt.as.for_l.cond, depth + 2);

        dump_depth(depth + 1);
        printf("StmtKindForFinishingStmt\n");
        dump_stmt_depth(*stmt.as.for_l.fstmt, depth + 2);

        dump_depth(depth + 1);
        printf("StmtKindForBody\n");
        dump_stmt_depth(*stmt.as.for_l.body, depth + 2);
        break;
    case STMT_KIND_WHILE:
        printf("StmtKindWhile "FLNOLFmt"\n", FLArg(stmt.loc));

        dump_depth(depth + 1);
        printf("StmtKindWhileCondition\n");
        dump_expr_depth(stmt.as.while_l.cond, depth + 2);

        dump_depth(depth + 1);
        printf("StmtKindWhileBody\n");
        dump_stmt_depth(*stmt.as.while_l.body, depth + 2);
        break;
    case STMT_KIND_EXPR:
        printf("StmtKindExpr "FLNOLFmt"\n", FLArg(stmt.loc));

        dump_expr_depth(stmt.as.expr, depth + 1);
        break;
    case STMT_KIND_RETURN:
        printf("StmtKindReturn "FLNOLFmt"\n", FLArg(stmt.loc));

        dump_expr_depth(stmt.as.expr, depth + 1);
        break;
    case STMT_KIND_BREAK:
        printf("StmtKindBreak "FLNOLFmt"\n", FLArg(stmt.loc));
        break;
    case STMT_KIND_CONTINUE:
        printf("StmtKindContinue "FLNOLFmt"\n", FLArg(stmt.loc));
        break;
    case STMT_KIND_COMPOUND:
        printf("StmtKindCompound "FLNOLFmt"\n", FLArg(stmt.loc));

        for (size_t i = 0; i < stmt.as.compound->items_count; ++i)
            dump_stmt_depth(*((Stmt *)stmt.as.compound->items[i]), depth + 1);
        break;
    default:
        assert(false && "Unreachable.");
    }
}

void dump_stmt(Stmt stmt)
{
    dump_stmt_depth(stmt, 0);
}

static_assert(TOPS_COUNT == 4,
              "Top statements count has changed, "
              "please update the function below.");

void dump_top_depth(Top top, size_t depth)
{
    dump_depth(depth);

    switch (top.kind) {
    case TOP_KIND_FUN_DEF:
        printf("TopKindFunDef (name: "STRFmt", type: %s) "FLNOLFmt"\n",
               STRArg(top.as.fundef.name), top.as.fundef.ret_type.name, FLArg(top.loc));
        if (top.as.fundef.args->items_count > 0) {
            dump_depth(depth + 1);
            printf("TopKindFunDefArgs\n");

            for (size_t i = 0; i < top.as.fundef.args->items_count; ++i) {
                dump_depth(depth + 2);
                printf("TopKindFunDefArg (type: %s) "FLNOLFmt"\n",
                       ((VarDef *)top.as.fundef.args->items[i])->type.name,
                       FLArg(((VarDef *)top.as.fundef.args->items[i])->loc));

                dump_depth(depth + 3);
                printf("TopKindFunDefArgExpr\n");
                dump_expr_depth(((VarDef *)top.as.fundef.args->items[i])->var_expr, depth + 4);
            }
        }
        dump_stmt_depth(top.as.fundef.body, depth + 1);
        break;
    case TOP_KIND_VAR_DEF:
        printf("TopKindVarDef (type: %s) "FLNOLFmt"\n",
               top.as.var_def.type.name, FLArg(top.loc));
        dump_depth(depth + 1);
        printf("TopKindVarDefExpr\n");
        dump_expr_depth(top.as.var_def.var_expr, depth + 2);

        if (top.as.var_def.has_init) {
            dump_depth(depth + 1);
            printf("TopKindVarDefInit\n");
            dump_expr_depth(top.as.var_def.init, depth + 2);
        }
        break;
    case TOP_KIND_INCLUDE:
        printf("TopKindInclude "FLNOLFmt"\n", FLArg(top.loc));
        break;
    case TOP_KIND_MACRO:
        return;
    default:
        assert(false && "Unreachable.");
    }
}

void dump_top(Top top)
{
    dump_top_depth(top, 0);
}
