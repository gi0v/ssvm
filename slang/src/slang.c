#include <stdio.h>

#include <sprs.h>
#include <slexer.h>
#include <scompiler.h>

#define LIST_IMPLEMENTATION
#include <list.h>
#undef LIST_IMPLEMENTATION

#define STRLIB_IMPLEMENTATION
#include <strlib.h>
#undef STRLIB_IMPLEMENTATION

#define SSVM_IMPLEMENTATION
#include <ssvm.h>
#undef SSVM_IMPLEMENTATION

#define DEFAULT_MEM_SZ (size_t) 640000

void help(FILE * restrict stream, const char * const pn)
{
    fprintf(stream, "USAGE: %s [OPTIONS] [INPUT (.sl)]\n", pn);
    fprintf(stream, "OPTIONS:\n");
    fprintf(stream, "    -h,                    Displays this message.\n");
    fprintf(stream, "    -o [OUTPUT (.sbc)],    Specify the output file name.\n");
    fprintf(stream, "    -ast,                  Dumps the program AST while compiling it.\n");
    fprintf(stream, "    -r,                    Runs the program instead of saving it to a file.\n");
    fprintf(stream, "    -m,                    Set memory size for program, default is %zu.\n", DEFAULT_MEM_SZ);
}

char *shift(int *argc, char ***argv)
{
    assert(*argc > 0);

    char *r = **argv;
    
    *argc -= 1;
    *argv += 1;

    return r;
}

int main(int argc, char **argv)
{
    size_t mem_sz = DEFAULT_MEM_SZ;
    Lexer *lexer = NULL;
    bool free_of = false;
    bool dump_ast = false;
    bool run_program = false;
    const char *input_file = NULL;
    const char *output_file = NULL;
    const char * const program_name = shift(&argc, &argv);

    if (argc < 1) { help(stderr, program_name); exit(1); }

    while (argc) {
        char *flag = shift(&argc, &argv);

        if (!strcmp(flag, "-h")) { help(stdout, program_name); exit(0); }
        else if (!strcmp(flag, "-o")) output_file = shift(&argc, &argv);
        else if (!strcmp(flag, "-ast")) dump_ast = true;
        else if (!strcmp(flag, "-r")) run_program = true;
        else if (!strcmp(flag, "-m")) mem_sz = strtoull(shift(&argc, &argv), NULL, 10);
        else input_file = flag;
    }

    ASSERT(input_file != NULL, INTERNAL_FL, "no input file specified.");

    if (output_file == NULL) {
        String of_str = {0};
        String if_str = str_from_static_cstr(input_file);

        size_t i = 0;

        if (str_find_char_right(if_str, '.', &i)) {
            str_free(str_chop_right(&if_str, if_str.length - (i + 1)));
        }

        of_str = if_str;
        of_str = str_append(of_str, ".sbc");
        output_file = STR_CSTR(of_str);
        free_of = true;
    }

    macros = init_list(sizeof(Macro *));
    lexer = init_lexer(input_file);

    if (lexer == NULL)
        log_msg(INTERNAL_FL, LOG_LVL_ERR, "failed to open file `%s`", input_file);

    compile_program_to_file(lexer, output_file, mem_sz, dump_ast, run_program);

    if (free_of) free((char *) output_file);

    for (size_t i = 0; i < macros->items_count; ++i) {
        str_free(((Macro *)macros->items[i])->src);
        str_free(((Macro *)macros->items[i])->name);
        free(macros->items[i]);
    } free_list(macros);

    free_lexer(lexer);

    if (error_hit) exit(1);

    return 0;
}
