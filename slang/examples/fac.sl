func fac(n: u64): u64
{
    if n == 0 return 1;

    return n * fac(n - 1);
}

func main(): u0
{
    for var i: u64 = 0; i <= 10; i += 1; {
        dump(fac(i));
    }
}
