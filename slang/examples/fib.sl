func fib(n: u64): u64
{
    if n == 0 return 0;
    else if n == 1 return 1;
    else return fib(n - 1) + fib(n - 2);
}

func main(): u0
{
    for var i: u64 = 1; i <= 19; i += 1; {
        dump(fib(i));
    }
}
