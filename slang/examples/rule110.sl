func main() : u0
{
    var board[30]: u8;
    var board_cap: u8 = 30;
    
    board[board_cap - 2] = 1; // Starting point
    
    for var i: u8 = 0; i < board_cap - 2; i += 1; {
        var j: u8;

        for j = 0; j < board_cap; j += 1; {
            if board[j] == 0 write(" "); else write("*");
        }
        
        write("\n");
        
        var pattern: u8 = (board[0] << 1) | board[1];
        j = 1;
        
        for j = 0; j < board_cap - 1; j += 1; {
            pattern = ((pattern << 1) & 7) | board[j + 1];
            board[j] = (110 >> pattern) & 1;
        }
    }
}
