func main(): u0
{
    for var i: u64 = 1; i <= 100; i += 1; {
        var _: u8 = 0;

        if i % 3 == 0 { _ = 1; write("Fizz"); }
        if i % 5 == 0 { _ = 1; write("Buzz"); }

        if _ write("\n");
        else dump(i);
    }
}
