(defvar slang-mode-syntax-table
  (let ((table (make-syntax-table)))
    ;; comments
    (modify-syntax-entry ?/ ". 124b")
    (modify-syntax-entry ?* ". 23")
    (modify-syntax-entry ?\n "> b")
    ;; preprocessor
    (modify-syntax-entry ?@ "." table)
    ;; chars = strings
    (modify-syntax-entry ?' "\"" table)
    table))

(eval-and-compile
  (defconst slang-keywords '("if" "else" "for" "while" "cast" "break" "continue" "var" "func" "return"))
  (defconst slang-types '("u0" "u8" "u16" "u32" "u64"))
  (defconst slang-functions '("main"))
  (defconst slang-events '("dump" "write")))

(defconst slang-highlights
  `(
    (,(regexp-opt slang-keywords 'symbols) . font-lock-keyword-face)
    (,(regexp-opt slang-types 'symbols) . font-lock-type-face)
    (,(regexp-opt slang-functions 'symbols) . font-lock-function-name-face)
    (,(regexp-opt slang-events 'symbols) . font-lock-builtin-face)
    ("@ *[a-zA-Z0-9_]+" . font-lock-preprocessor-face)
    ))

(defun slang--space-prefix-len (line)
  (- (length line)
     (length (string-trim-left line))))

(defun slang--previous-non-empty-line ()
  (save-excursion
    (forward-line -1)
    (while (and (not (bobp))
                (string-empty-p
                 (string-trim-right
                  (thing-at-point 'line t))))
      (forward-line -1))
    (thing-at-point 'line t)))

(defun slang--desired-indentation ()
  (let ((cur-line (string-trim-right (thing-at-point 'line t)))
        (prev-line (string-trim-right (slang--previous-non-empty-line)))
        (indent-len 4))
    (cond
     ((and (string-suffix-p "{" prev-line)
           (string-prefix-p "}" (string-trim-left cur-line)))
      (slang--space-prefix-len prev-line))
     ((string-suffix-p "{" prev-line)
      (+ (slang--space-prefix-len prev-line) indent-len))
     ((string-prefix-p "}" (string-trim-left cur-line))
      (max (- (slang--space-prefix-len prev-line) indent-len) 0))
     (t (slang--space-prefix-len prev-line)))))

(defun slang-indent-line ()
  (interactive)
  (when (not (bobp))
    (let* ((current-indentation
            (slang--space-prefix-len (thing-at-point 'line t)))
           (desired-indentation
            (slang--desired-indentation))
           (n (max (- (current-column) current-indentation) 0)))
      (indent-line-to desired-indentation)
      (forward-char n))))

;;;###autoload
(define-derived-mode slang-mode prog-mode "slang"
  "Major Mode for editing Slang source code."
  :syntax-table slang-mode-syntax-table
  (setq font-lock-defaults '(slang-highlights))
  (setq tab-width 2)
  (setq-local indent-line-function 'slang-indent-line)
  (setq-local comment-start "# ")
  (setq-local comment-end ""))

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.sl\\'" . slang-mode))

(provide 'slang-mode)
