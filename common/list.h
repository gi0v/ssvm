#ifndef _LIST_H
#define _LIST_H

#include <stddef.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct {
    void **items;
    size_t item_sz, items_count;
} List;

List *init_list(size_t item_sz);
void free_list(List *l);
void *add_to_list(List *l, void *x);

#endif  // _LIST_H

#ifdef LIST_IMPLEMENTATION

List *init_list(size_t item_sz)
{
    List *l = calloc(1, sizeof(*l));

    l->item_sz = item_sz;
    l->items_count = 0;
    l->items = calloc(l->items_count + 1, item_sz);

    return l;
}

void free_list(List *l)
{
    free(l->items);
    free(l);
}

void *add_to_list(List *l, void *x)
{
    l->items = realloc(l->items, l->item_sz * (l->items_count + 1));
    l->items[l->items_count] = x;
    l->items_count++;

    return l->items[l->items_count - 1];
}

bool is_in_list(List *l, void *x)
{
    for (size_t i = 0; i < l->items_count; ++i)
        if (l->items[i] == x)
            return true;

    return false;
}

#endif // LIST_IMPLEMENTATION
