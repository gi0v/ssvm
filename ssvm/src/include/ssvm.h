#ifndef _SSVM_H
#define _SSVM_H

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>
#include <stdbool.h>

#include <list.h>

// TODO: Maybe make this more elegant?
#define NAT_DUMP 0
#define NAT_WRITE 1

#define STACK_CAP 1024

#define COST_CALC(nparams) 1 + (8 * nparams)
#define COST_CALC_INV(cost) (i - 1) / 8
#define ALLOC_NATIVE(ssvm, fname)               \
    do {                                        \
        ssvm_nat *nat = calloc(1, sizeof *nat); \
        nat->exec = fname;                      \
        add_to_list(ssvm->natives, nat);        \
    } while (false)                             \

#define FIX_ENDIANNESS(x)                                       \
    do {                                                        \
        x = ((x << 8) & 0xFF00FF00FF00FF00ULL )                 \
            | ((x >> 8) & 0x00FF00FF00FF00FFULL );              \
        x = ((x << 16) & 0xFFFF0000FFFF0000ULL )                \
            | ((x >> 16) & 0x0000FFFF0000FFFFULL );             \
        x = (x << 32) | (x >> 32);                              \
    } while (false)                                             \

#define SET_ARRAY64_ENDIANNESS(arr, i, val)                             \
    do {                                                                \
        FIX_ENDIANNESS(val);                                            \
        memcpy(&arr[i], &val, sizeof(ssvm_word));                       \
    } while (false)                                                     \

typedef uint64_t ssvm_word;
typedef uint8_t  ssvm_byte;

#define PROG_MAGIC "SSVM"
#define PROG_MAGIC_SZ 4

typedef struct {
    size_t prog_sz;
    ssvm_byte *prog;
    ssvm_word curr_addr, instr_addr;
} ssvm_cgen;

typedef struct {
    char magic[PROG_MAGIC_SZ];
    size_t prog_sz, mem_sz;
    ssvm_byte *prog;
} ssvm_prog;

typedef struct {
    bool       hlt;                // Was a hlt hit?
    List       *natives;           // Native functions, TODO: make it so that you can load .so files into natives.
    List       *addr_table;        // Conversion table: { program addresses -> instructions count }
    size_t     mem_sz, program_sz; // Memory & Code size.
    ssvm_byte  *mem;               // Program memory.
    ssvm_byte  *program;           // Decided by input file.
    ssvm_word  pc, sp;             // Program Counter & Stack Pointer.
    ssvm_word  stack[STACK_CAP];   // Capped.
} ssvm;

typedef enum {
    INSTR_PUSH = 0xA0,
    INSTR_ADD,
    INSTR_SUB,
    INSTR_MUL,
    INSTR_DIV,
    INSTR_REM,
    INSTR_EQU,
    INSTR_NEQ,
    INSTR_GRE,
    INSTR_LES,
    INSTR_GEQ,
    INSTR_LEQ,
    INSTR_BAND,
    INSTR_BOR,
    INSTR_BXOR,
    INSTR_BNOT,
    INSTR_LAND,
    INSTR_LOR,
    INSTR_LNOT,
    INSTR_SHL,
    INSTR_SHR,
    INSTR_JMP,
    INSTR_JIF,
    INSTR_JSR,
    INSTR_DUP,
    INSTR_SWAP,
    INSTR_DROP,
    INSTR_OVER,
    INSTR_ROT,
    INSTR_RET,
    INSTR_NOP,
    INSTR_WR8,
    INSTR_WR16,
    INSTR_WR32,
    INSTR_WR64,
    INSTR_RD8,
    INSTR_RD16,
    INSTR_RD32,
    INSTR_RD64,
    INSTR_NAT,
    INSTR_HLT,
    INSTR_LAST, // This has to be the last.
} ssvm_instr_kind;

#define INSTR_COUNT INSTR_LAST - INSTR_PUSH

typedef struct {
    void (*exec)(ssvm *ssvm);
} ssvm_nat;

typedef struct {
    char *name;
    ssvm_instr_kind kind;
    ssvm_word cost;       /* Cost ::= Level of increment of instruction pointer
                             (e.g.) if we have an instuction (1 byte)
                             with a single parameter (8 bytes)
                             the the cost will be 9 ( = 8 + 1)
                             so, we'll have pc += 9 or pc += instr.cost */
    void (*exec)(ssvm *ssvm);
} ssvm_instr;

#define GET_WORD(arr, i)                                  \
    (((ssvm_word)((arr)[i + 7]) <<  0)                    \
     + ((ssvm_word)((arr)[i + 6]) <<  8)                  \
     + ((ssvm_word)((arr)[i + 5]) << 16)                  \
     + ((ssvm_word)((arr)[i + 4]) << 24)                  \
     + ((ssvm_word)((arr)[i + 3]) << 32)                  \
     + ((ssvm_word)((arr)[i + 2]) << 40)                  \
     + ((ssvm_word)((arr)[i + 1]) << 48)                  \
     + ((ssvm_word)((arr)[i + 0]) << 56))                 \

#define GET_PARAM(ssvm, argi) GET_WORD(ssvm->program, (ssvm->pc + 1) + (8 * argi))

ssvm *init_ssvm(size_t mem_sz);
void free_ssvm(ssvm *ssvm);

ssvm_cgen *init_cgen();
void free_cgen(ssvm_cgen *cgen);
void push_instr(ssvm_cgen *cgen, ssvm_byte instr);
void push_instr_arg(ssvm_cgen *cgen, ssvm_word arg);
ssvm_prog fill_prog(ssvm_byte *prog, size_t prog_sz, size_t mem_sz);
void free_prog(ssvm_prog p);
void wr_cgen_to_file(ssvm_cgen *cgen, const char *fn, size_t mem_sz);
void wr_prog_to_file(ssvm_prog prog, const char *fn);
ssvm_prog ld_ssvm_file(const char *fn);
void ld_ssvm_program(ssvm *ssvm, ssvm_byte *program, size_t program_sz);
void fwd_instr(ssvm *ssvm);
void exec_until_hlt(ssvm *ssvm);

void ssvm_exec_push(ssvm *ssvm);
void ssvm_exec_add(ssvm *ssvm);
void ssvm_exec_sub(ssvm *ssvm);
void ssvm_exec_mul(ssvm *ssvm);
void ssvm_exec_div(ssvm *ssvm);
void ssvm_exec_equ(ssvm *ssvm);
void ssvm_exec_neq(ssvm *ssvm);
void ssvm_exec_gre(ssvm *ssvm);
void ssvm_exec_les(ssvm *ssvm);
void ssvm_exec_geq(ssvm *ssvm);
void ssvm_exec_leq(ssvm *ssvm);
void ssvm_exec_add(ssvm *ssvm);
void ssvm_exec_sub(ssvm *ssvm);
void ssvm_exec_mul(ssvm *ssvm);
void ssvm_exec_div(ssvm *ssvm);
void ssvm_exec_rem(ssvm *ssvm);
void ssvm_exec_equ(ssvm *ssvm);
void ssvm_exec_neq(ssvm *ssvm);
void ssvm_exec_gre(ssvm *ssvm);
void ssvm_exec_les(ssvm *ssvm);
void ssvm_exec_geq(ssvm *ssvm);
void ssvm_exec_leq(ssvm *ssvm);
void ssvm_exec_band(ssvm *ssvm);
void ssvm_exec_bor(ssvm *ssvm);
void ssvm_exec_bxor(ssvm *ssvm);
void ssvm_exec_land(ssvm *ssvm);
void ssvm_exec_lor(ssvm *ssvm);
void ssvm_exec_shl(ssvm *ssvm);
void ssvm_exec_shr(ssvm *ssvm);
void ssvm_exec_bnot(ssvm *ssvm);
void ssvm_exec_lnot(ssvm *ssvm);
void ssvm_exec_jmp(ssvm *ssvm);
void ssvm_exec_jif(ssvm *ssvm);
void ssvm_exec_jsr(ssvm *ssvm);
void ssvm_exec_dup(ssvm *ssvm);
void ssvm_exec_swap(ssvm *ssvm);
void ssvm_exec_drop(ssvm *ssvm);
void ssvm_exec_over(ssvm *ssvm);
void ssvm_exec_rot(ssvm *ssvm);
void ssvm_exec_ret(ssvm *ssvm);
void ssvm_exec_nop(ssvm *ssvm);
void ssvm_exec_wr8(ssvm *ssvm);
void ssvm_exec_wr16(ssvm *ssvm);
void ssvm_exec_wr32(ssvm *ssvm);
void ssvm_exec_wr64(ssvm *ssvm);
void ssvm_exec_rd8(ssvm *ssvm);
void ssvm_exec_rd16(ssvm *ssvm);
void ssvm_exec_rd32(ssvm *ssvm);
void ssvm_exec_rd64(ssvm *ssvm);
void ssvm_exec_nat(ssvm *ssvm);
void ssvm_exec_hlt(ssvm *ssvm);

void ssvm_nat_dump(ssvm *ssvm);
void ssvm_nat_write(ssvm *ssvm);

#endif  // _SSVM_H

#ifdef SSVM_IMPLEMENTATION

static_assert(INSTR_COUNT == 41, "Instruction count changed, please update the definitions below this assertion.");

ssvm_instr ssvm_to_instr[] = {
    [INSTR_PUSH] = { .kind = INSTR_PUSH, .name = "push", .cost = COST_CALC(1), .exec = ssvm_exec_push },
    [INSTR_ADD]  = { .kind = INSTR_ADD,  .name = "add",  .cost = COST_CALC(0), .exec = ssvm_exec_add  },
    [INSTR_SUB]  = { .kind = INSTR_SUB,  .name = "sub",  .cost = COST_CALC(0), .exec = ssvm_exec_sub  },
    [INSTR_MUL]  = { .kind = INSTR_MUL,  .name = "mul",  .cost = COST_CALC(0), .exec = ssvm_exec_mul  },
    [INSTR_DIV]  = { .kind = INSTR_DIV,  .name = "div",  .cost = COST_CALC(0), .exec = ssvm_exec_div  },
    [INSTR_REM]  = { .kind = INSTR_REM,  .name = "rem",  .cost = COST_CALC(0), .exec = ssvm_exec_rem  },
    [INSTR_EQU]  = { .kind = INSTR_EQU,  .name = "equ",  .cost = COST_CALC(0), .exec = ssvm_exec_equ  },
    [INSTR_NEQ]  = { .kind = INSTR_NEQ,  .name = "neq",  .cost = COST_CALC(0), .exec = ssvm_exec_neq  },
    [INSTR_GRE]  = { .kind = INSTR_GRE,  .name = "gre",  .cost = COST_CALC(0), .exec = ssvm_exec_gre  },
    [INSTR_LES]  = { .kind = INSTR_LES,  .name = "les",  .cost = COST_CALC(0), .exec = ssvm_exec_les  },
    [INSTR_GEQ]  = { .kind = INSTR_GEQ,  .name = "geq",  .cost = COST_CALC(0), .exec = ssvm_exec_geq  },
    [INSTR_LEQ]  = { .kind = INSTR_LEQ,  .name = "leq",  .cost = COST_CALC(0), .exec = ssvm_exec_leq  },
    [INSTR_BAND] = { .kind = INSTR_BAND, .name = "band", .cost = COST_CALC(0), .exec = ssvm_exec_band },
    [INSTR_BOR]  = { .kind = INSTR_BOR,  .name = "bor",  .cost = COST_CALC(0), .exec = ssvm_exec_bor  },
    [INSTR_BXOR] = { .kind = INSTR_BXOR, .name = "bxor", .cost = COST_CALC(0), .exec = ssvm_exec_bxor },
    [INSTR_BNOT] = { .kind = INSTR_BNOT, .name = "bnot", .cost = COST_CALC(0), .exec = ssvm_exec_bnot },
    [INSTR_LAND] = { .kind = INSTR_LAND, .name = "land", .cost = COST_CALC(0), .exec = ssvm_exec_land },
    [INSTR_LOR]  = { .kind = INSTR_LOR,  .name = "lor",  .cost = COST_CALC(0), .exec = ssvm_exec_lor  },
    [INSTR_LNOT] = { .kind = INSTR_LNOT, .name = "lnot", .cost = COST_CALC(0), .exec = ssvm_exec_lnot },
    [INSTR_SHL]  = { .kind = INSTR_SHL,  .name = "shl",  .cost = COST_CALC(0), .exec = ssvm_exec_shl  },
    [INSTR_SHR]  = { .kind = INSTR_SHR,  .name = "shr",  .cost = COST_CALC(0), .exec = ssvm_exec_shr  },
    [INSTR_JMP]  = { .kind = INSTR_JMP,  .name = "jmp",  .cost = COST_CALC(1), .exec = ssvm_exec_jmp  },
    [INSTR_JIF]  = { .kind = INSTR_JIF,  .name = "jif",  .cost = COST_CALC(1), .exec = ssvm_exec_jif  },
    [INSTR_JSR]  = { .kind = INSTR_JSR,  .name = "jsr",  .cost = COST_CALC(1), .exec = ssvm_exec_jsr  },
    [INSTR_DUP]  = { .kind = INSTR_DUP,  .name = "dup",  .cost = COST_CALC(1), .exec = ssvm_exec_dup  },
    [INSTR_SWAP] = { .kind = INSTR_SWAP, .name = "swap", .cost = COST_CALC(0), .exec = ssvm_exec_swap },
    [INSTR_DROP] = { .kind = INSTR_DROP, .name = "drop", .cost = COST_CALC(1), .exec = ssvm_exec_drop },
    [INSTR_OVER] = { .kind = INSTR_OVER, .name = "over", .cost = COST_CALC(0), .exec = ssvm_exec_over },
    [INSTR_ROT]  = { .kind = INSTR_ROT,  .name = "rot",  .cost = COST_CALC(0), .exec = ssvm_exec_rot  },
    [INSTR_RET]  = { .kind = INSTR_RET,  .name = "ret",  .cost = COST_CALC(0), .exec = ssvm_exec_ret  },
    [INSTR_NOP]  = { .kind = INSTR_NOP,  .name = "nop",  .cost = COST_CALC(0), .exec = ssvm_exec_nop  },
    [INSTR_WR8]  = { .kind = INSTR_WR8,  .name = "wr8",  .cost = COST_CALC(0), .exec = ssvm_exec_wr8  },
    [INSTR_WR16] = { .kind = INSTR_WR16, .name = "wr16", .cost = COST_CALC(0), .exec = ssvm_exec_wr16 },
    [INSTR_WR32] = { .kind = INSTR_WR32, .name = "wr32", .cost = COST_CALC(0), .exec = ssvm_exec_wr32 },
    [INSTR_WR64] = { .kind = INSTR_WR64, .name = "wr64", .cost = COST_CALC(0), .exec = ssvm_exec_wr64 },
    [INSTR_RD8]  = { .kind = INSTR_RD8,  .name = "rd8",  .cost = COST_CALC(0), .exec = ssvm_exec_rd8  },
    [INSTR_RD16] = { .kind = INSTR_RD16, .name = "rd16", .cost = COST_CALC(0), .exec = ssvm_exec_rd16 },
    [INSTR_RD32] = { .kind = INSTR_RD32, .name = "rd32", .cost = COST_CALC(0), .exec = ssvm_exec_rd32 },
    [INSTR_RD64] = { .kind = INSTR_RD64, .name = "rd64", .cost = COST_CALC(0), .exec = ssvm_exec_rd64 },
    [INSTR_NAT]  = { .kind = INSTR_NAT,  .name = "nat",  .cost = COST_CALC(1), .exec = ssvm_exec_nat  },
    [INSTR_HLT]  = { .kind = INSTR_HLT,  .name = "hlt",  .cost = COST_CALC(0), .exec = ssvm_exec_hlt  },
};

ssvm_word ssvm_stack_pop(ssvm *ssvm)
{
    if ((ssvm)->sp == 0) {
        fprintf(stderr, "VM_ERROR (0x%016lX): Stack underflow.\n", ssvm->pc);
        exit(1);
    }
    
    ssvm->sp--;
    
    ssvm_word res = ssvm->stack[ssvm->sp];
    
    ssvm->stack[ssvm->sp] = 0;
    
    return res;
}

void scan_program_addrs(ssvm *ssvm)
{
    ssvm_word i = 0;
    
    while (i < ssvm->program_sz) {
        i += ssvm_to_instr[ssvm->program[i]].cost;
        add_to_list(ssvm->addr_table, (void *) i);
    }
}

ssvm *init_ssvm(size_t mem_sz)
{
    ssvm *ssvm = calloc(1, sizeof *ssvm);
    
    ssvm->hlt = false;
    ssvm->mem = calloc(mem_sz, sizeof(ssvm_byte));
    ssvm->mem_sz = mem_sz;
    ssvm->natives = init_list(sizeof(ssvm_nat));
    ssvm->addr_table = init_list(sizeof(ssvm_word));
    add_to_list(ssvm->addr_table, 0);

    ALLOC_NATIVE(ssvm, ssvm_nat_dump);
    ALLOC_NATIVE(ssvm, ssvm_nat_write);
    
    return ssvm;
}

void free_ssvm(ssvm *ssvm)
{
    free(ssvm->mem);
    free(ssvm->program);

    for (size_t i = 0; i < ssvm->natives->items_count; ++i)
        free(ssvm->natives->items[i]);

    free_list(ssvm->natives);
    free_list(ssvm->addr_table);
    free(ssvm);
}

ssvm_cgen *init_cgen()
{
    ssvm_cgen *cgen = calloc(1, sizeof(*cgen));

    cgen->prog = calloc(1, sizeof(ssvm_byte));
    cgen->prog_sz = 0;
    cgen->curr_addr = 0;
    cgen->instr_addr = 0;

    return cgen;
}

void free_cgen(ssvm_cgen *cgen)
{
    free(cgen->prog);
    free(cgen);
}

void push_instr(ssvm_cgen *cgen, ssvm_byte instr)
{
    cgen->curr_addr += sizeof(ssvm_byte);
    cgen->instr_addr += sizeof(ssvm_byte);
    cgen->prog_sz += sizeof(ssvm_byte);
    cgen->prog = realloc(cgen->prog, cgen->prog_sz * sizeof(ssvm_byte));
    cgen->prog[cgen->prog_sz - sizeof(ssvm_byte)] = instr;
}

void push_instr_arg(ssvm_cgen *cgen, ssvm_word arg)
{
    cgen->curr_addr += sizeof(ssvm_word);
    cgen->prog_sz += sizeof(ssvm_word);
    cgen->prog = realloc(cgen->prog, cgen->prog_sz * sizeof(ssvm_byte));

    SET_ARRAY64_ENDIANNESS(cgen->prog, cgen->prog_sz - sizeof(ssvm_word), arg);
}

ssvm_prog fill_prog(ssvm_byte *prog, size_t prog_sz, size_t mem_sz)
{
    ssvm_prog p = {0};

    p.prog = calloc(prog_sz, sizeof(ssvm_byte));

    memcpy(p.magic, PROG_MAGIC, PROG_MAGIC_SZ);
    memcpy(p.prog, prog, prog_sz);
    p.prog_sz = prog_sz;
    p.mem_sz = mem_sz;

    return p;
}

void free_prog(ssvm_prog p) { free(p.prog); }

void wr_cgen_to_file(ssvm_cgen *cgen, const char *fn, size_t mem_sz)
{
    ssvm_prog prog = fill_prog(cgen->prog, cgen->prog_sz, mem_sz);

    wr_prog_to_file(prog, fn);

    free_prog(prog);
}

void wr_prog_to_file(ssvm_prog prog, const char *fn)
{
    FILE *fp = fopen(fn, "w+");

    assert(fp != NULL);

    fprintf(fp, "%s%016zu%016zu", prog.magic, prog.prog_sz, prog.mem_sz);
    fwrite(prog.prog, prog.prog_sz, sizeof(ssvm_byte), fp);

    fclose(fp);
}

ssvm_prog ld_ssvm_file(const char *fn)
{
    FILE *fp = fopen(fn, "r");
    ssvm_prog prog = {0};

    assert(fp != NULL);

    fread(prog.magic, PROG_MAGIC_SZ, sizeof(char), fp);
    fscanf(fp, "%016zu%016zu", &prog.prog_sz, &prog.mem_sz);

    prog.prog = calloc(prog.prog_sz, sizeof(ssvm_byte));

    fread(prog.prog, prog.prog_sz, sizeof(ssvm_byte), fp);

    fclose(fp);

    assert(strcmp(PROG_MAGIC, prog.magic) == 0);

    return prog;
}

void ld_ssvm_program(ssvm *ssvm, ssvm_byte *program, size_t program_sz)
{
    ssvm->program = calloc(program_sz, sizeof(ssvm_byte));
    ssvm->program_sz = program_sz;
    memcpy(ssvm->program, program, program_sz);
    scan_program_addrs(ssvm);
}

void fwd_instr(ssvm *ssvm)
{
    ssvm_instr instr = ssvm_to_instr[ssvm->program[ssvm->pc]];

#ifdef SSVM_DEBUG
    for (size_t i = 0; i < ssvm->addr_table->items_count; ++i)
        if ((ssvm_word) ssvm->addr_table->items[i] == ssvm->pc)
            printf("0x%016lX:\t", (ssvm_word) ssvm->addr_table->items[i]);

    printf("%s\t", instr.name);

    for (size_t i = COST_CALC(0); i < instr.cost; i += COST_CALC(1))
        printf(" %lu", GET_PARAM(ssvm, COST_CALC_INV(i)));

    printf("\n");
#endif

    instr.exec(ssvm);

    ssvm->pc += instr.cost;
}

void exec_until_hlt(ssvm *ssvm)
{
    while (!ssvm->hlt) {
        if (ssvm->pc >= ssvm->program_sz) {
            fprintf(stderr, "VM_ERROR (0x%016lX): Program Counter exceeded given program size.\n", ssvm->pc);
            exit(1);
        }
        
        fwd_instr(ssvm);
    }
}

/// Instructions
/// ============

#define STACK_PUSH(ssvm, val)                                           \
    do {                                                                \
        if ((ssvm)->sp + 1 > STACK_CAP) {                               \
            fprintf(stderr, "VM_ERROR (0x%016lX): Stack overflow.\n",   \
                    ssvm->pc);                                          \
            exit(1);                                                    \
        }                                                               \
                                                                        \
        (ssvm)->stack[(ssvm)->sp] = val;                                \
        (ssvm)->sp++;                                                   \
    } while(false)                                                      \
        
#define STACK_PUSH_BIN_OP(ssvm, op)                \
    do {                                           \
        ssvm_word b = ssvm_stack_pop((ssvm));      \
        ssvm_word a = ssvm_stack_pop((ssvm));      \
        STACK_PUSH((ssvm), a op b);                \
    } while(false)                                 \
        
#define STACK_PUSH_UNARY_OP(ssvm, op) STACK_PUSH((ssvm), op (ssvm_stack_pop((ssvm))))

#define WRITE_BYTES(ssvm, type)                                         \
    do {                                                                \
        type value = (type) ssvm_stack_pop(ssvm);                       \
        ssvm_word addr = ssvm_stack_pop(ssvm);                          \
        if (addr > (ssvm->mem_sz - sizeof(type) - 1)) {                 \
            fprintf(stderr, "VM_ERROR (0x%016lX): "                      \
                    "Illegal memory access (0x%016lX).\n",               \
                    ssvm->pc, addr);                                    \
            exit(1);                                                    \
        }                                                               \
        memcpy(&ssvm->mem[addr], &value, sizeof(value));                \
    } while(false)                                                      \
        
#define READ_BYTES(ssvm, type)                                          \
    do {                                                                \
        ssvm_word addr = ssvm_stack_pop(ssvm);                          \
        if (addr >= ssvm->mem_sz) {                                     \
            fprintf(stderr, "VM_ERROR (0x%016lX): "                      \
                    "Illegal memory access (0x%016lX).\n",               \
                    ssvm->pc, addr);                                    \
            exit(1);                                                    \
        }                                                               \
        type tmp;                                                       \
        memcpy(&tmp, &ssvm->mem[addr], sizeof(type));                   \
        STACK_PUSH(ssvm, tmp);                                          \
    } while (false)                                                     \

void ssvm_exec_push(ssvm *ssvm) { STACK_PUSH(ssvm, GET_PARAM(ssvm, 0)); }

void ssvm_exec_add(ssvm *ssvm)  { STACK_PUSH_BIN_OP(ssvm, +);  }
void ssvm_exec_sub(ssvm *ssvm)  { STACK_PUSH_BIN_OP(ssvm, -);  }
void ssvm_exec_mul(ssvm *ssvm)  { STACK_PUSH_BIN_OP(ssvm, *);  }
void ssvm_exec_div(ssvm *ssvm)  { STACK_PUSH_BIN_OP(ssvm, /);  }
void ssvm_exec_rem(ssvm *ssvm)  { STACK_PUSH_BIN_OP(ssvm, %);  }
void ssvm_exec_equ(ssvm *ssvm)  { STACK_PUSH_BIN_OP(ssvm, ==); }
void ssvm_exec_neq(ssvm *ssvm)  { STACK_PUSH_BIN_OP(ssvm, !=); }
void ssvm_exec_gre(ssvm *ssvm)  { STACK_PUSH_BIN_OP(ssvm, >);  }
void ssvm_exec_les(ssvm *ssvm)  { STACK_PUSH_BIN_OP(ssvm, <);  }
void ssvm_exec_geq(ssvm *ssvm)  { STACK_PUSH_BIN_OP(ssvm, >=); }
void ssvm_exec_leq(ssvm *ssvm)  { STACK_PUSH_BIN_OP(ssvm, <=); }
void ssvm_exec_band(ssvm *ssvm) { STACK_PUSH_BIN_OP(ssvm, &);  }
void ssvm_exec_bor(ssvm *ssvm)  { STACK_PUSH_BIN_OP(ssvm, |);  }
void ssvm_exec_bxor(ssvm *ssvm) { STACK_PUSH_BIN_OP(ssvm, ^);  }
void ssvm_exec_land(ssvm *ssvm) { STACK_PUSH_BIN_OP(ssvm, &&); }
void ssvm_exec_lor(ssvm *ssvm)  { STACK_PUSH_BIN_OP(ssvm, ||); }
void ssvm_exec_shl(ssvm *ssvm)  { STACK_PUSH_BIN_OP(ssvm, <<); }
void ssvm_exec_shr(ssvm *ssvm)  { STACK_PUSH_BIN_OP(ssvm, >>); }

void ssvm_exec_bnot(ssvm *ssvm) { STACK_PUSH_UNARY_OP(ssvm, ~); }
void ssvm_exec_lnot(ssvm *ssvm) { STACK_PUSH_UNARY_OP(ssvm, !); }

void ssvm_exec_jmp(ssvm *ssvm)
{
    ssvm_word addr = GET_PARAM(ssvm, 0);
    
    if (addr < ssvm->addr_table->items_count)
        ssvm->pc = (ssvm_word)(ssvm->addr_table->items[addr]) - ssvm_to_instr[INSTR_JMP].cost;
    else
        { fprintf(stderr, "VM_ERROR (0x%016lX): Invalid jump address (0x%016lX).\n", ssvm->pc, addr); exit(1); }
}
void ssvm_exec_jif(ssvm *ssvm) { if (ssvm_stack_pop(ssvm)) ssvm_exec_jmp(ssvm); }
void ssvm_exec_jsr(ssvm *ssvm) { STACK_PUSH(ssvm, ssvm->pc + ssvm_to_instr[INSTR_JSR].cost - 1); ssvm_exec_jmp(ssvm); }

void ssvm_exec_dup(ssvm *ssvm)
{
    ssvm_word n = ssvm_stack_pop(ssvm);
    ssvm_word max = GET_PARAM(ssvm, 0);
    
    STACK_PUSH(ssvm, n);
    
    for (ssvm_word i = 0; i <= max; ++i)
        STACK_PUSH(ssvm, n);
}

void ssvm_exec_swap(ssvm *ssvm)
{
    ssvm_word a = ssvm_stack_pop(ssvm);
    ssvm_word b = ssvm_stack_pop(ssvm);
    
    STACK_PUSH(ssvm, a);
    STACK_PUSH(ssvm, b);
}

void ssvm_exec_drop(ssvm *ssvm)
{
    ssvm_word max = GET_PARAM(ssvm, 0);
    
    for (ssvm_word i = 0; i <= max; ++i)
        ssvm_stack_pop(ssvm);
}

void ssvm_exec_over(ssvm *ssvm)
{
    ssvm_word a = ssvm_stack_pop(ssvm);
    ssvm_word b = ssvm_stack_pop(ssvm);
    
    STACK_PUSH(ssvm, b);
    STACK_PUSH(ssvm, a);
    STACK_PUSH(ssvm, b);
}

void ssvm_exec_rot(ssvm *ssvm)
{
    ssvm_word a = ssvm_stack_pop(ssvm);
    ssvm_word b = ssvm_stack_pop(ssvm);
    ssvm_word c = ssvm_stack_pop(ssvm);
    
    STACK_PUSH(ssvm, b);
    STACK_PUSH(ssvm, a);
    STACK_PUSH(ssvm, c);
}

void ssvm_exec_ret(ssvm *ssvm) { ssvm->pc = ssvm_stack_pop(ssvm); }

void ssvm_exec_nop(ssvm *ssvm)  { (void) ssvm; }

void ssvm_exec_wr8(ssvm *ssvm)
{
    ssvm_byte value = (ssvm_byte) ssvm_stack_pop(ssvm);
    ssvm_word addr = ssvm_stack_pop(ssvm);
    
    if (addr >= ssvm->mem_sz) {
        fprintf(stderr, "VM_ERROR (0x%016lX): Illegal memory access (0x%016lX).\n", ssvm->pc, addr);
        exit(1);
    }
    
    ssvm->mem[addr] = value;
}
void ssvm_exec_wr16(ssvm *ssvm) { WRITE_BYTES(ssvm, uint16_t); }
void ssvm_exec_wr32(ssvm *ssvm) { WRITE_BYTES(ssvm, uint32_t); }
void ssvm_exec_wr64(ssvm *ssvm) { WRITE_BYTES(ssvm, uint64_t); }

void ssvm_exec_rd8(ssvm *ssvm)  { READ_BYTES(ssvm, uint8_t); }
void ssvm_exec_rd16(ssvm *ssvm) { READ_BYTES(ssvm, uint16_t); }
void ssvm_exec_rd32(ssvm *ssvm) { READ_BYTES(ssvm, uint32_t); }
void ssvm_exec_rd64(ssvm *ssvm) { READ_BYTES(ssvm, uint64_t); }

void ssvm_exec_nat(ssvm *ssvm)
{
    ssvm_word n = GET_PARAM(ssvm, 0);

    if (n >= ssvm->natives->items_count) {
        fprintf(stderr, "VM_ERROR (0x%016lX): Invalid native (%lu).\n", ssvm->pc, n);
        exit(0);
    }

    ((ssvm_nat *)ssvm->natives->items[n])->exec(ssvm);
}

void ssvm_exec_hlt(ssvm *ssvm) { ssvm->hlt = true; }

void ssvm_nat_dump(ssvm *ssvm)
{
    printf("%lu\n", ssvm_stack_pop(ssvm));
}

void ssvm_nat_write(ssvm *ssvm)
{
    ssvm_word addr = ssvm_stack_pop(ssvm);

    while (ssvm->mem[addr] != 0) fputc(ssvm->mem[addr++], stdout);
}

#endif  // SSVM_IMPLEMENTATION
