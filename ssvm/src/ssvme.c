#define LIST_IMPLEMENTATION
#include <list.h>
#undef LIST_IMPLEMENTATION

// Define this for assembly output:
#define SSVM_DEBUG

#define SSVM_IMPLEMENTATION
#include <ssvm.h>
#undef SSVM_IMPLEMENTATION

int main(int argc, char **argv)
{
    assert(argc > 1);

    ssvm_prog prog = ld_ssvm_file(argv[1]);
    ssvm *ssvm = init_ssvm(prog.mem_sz);

    ld_ssvm_program(ssvm, prog.prog, prog.prog_sz);
    exec_until_hlt(ssvm);

    free_prog(prog);
    free_ssvm(ssvm);
    
    return 0;
}
